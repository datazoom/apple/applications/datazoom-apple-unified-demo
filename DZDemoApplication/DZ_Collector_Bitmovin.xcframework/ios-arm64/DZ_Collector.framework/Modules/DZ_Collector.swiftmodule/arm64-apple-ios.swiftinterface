// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.7.1 (swiftlang-5.7.1.135.3 clang-1400.0.29.51)
// swift-module-flags: -target arm64-apple-ios14.0 -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -module-name DZ_Collector
// swift-module-flags-ignorable: -enable-bare-slash-regex
import AVFoundation
import AVKit
import AdSupport
import AppTrackingTransparency
import BitmovinPlayerCore
import Foundation
import MobileCoreServices
import Swift
import SystemConfiguration
import UIKit
import UniformTypeIdentifiers
import _Concurrency
import _StringProcessing
import os
public struct DZEventError {
  public var code: Swift.String
  public var message: Swift.String
}
extension DZ_Collector.DZEventError : Swift.Encodable {
  public func encode(to encoder: Swift.Encoder) throws
}
public struct DZEventHeader {
  public mutating func initFromConfiguration(_ config: DZ_Collector.DZConfiguration?)
}
extension DZ_Collector.DZEventHeader : Swift.Encodable {
  public func encode(to encoder: Swift.Encoder) throws
}
public struct DZConfigurationEvents {
  public var content: [Swift.String]?
  public var ad: [Swift.String]?
}
extension DZ_Collector.DZConfigurationEvents : Swift.Decodable {
  public init(from decoder: Swift.Decoder) throws
}
public struct DZConfigurationEvent {
  public let name: Swift.String?
  public let mediaTypes: [Swift.String]?
}
extension DZ_Collector.DZConfigurationEvent : Swift.Decodable {
  public init(from decoder: Swift.Decoder) throws
}
public struct DZSamplingCriteriaParameter {
}
extension DZ_Collector.DZSamplingCriteriaParameter : Swift.Encodable {
  public func encode(to encoder: Swift.Encoder) throws
}
extension DZ_Collector.DZSamplingCriteriaParameter : Swift.Decodable {
  public init(from decoder: Swift.Decoder) throws
}
public struct DZEvent {
}
extension DZ_Collector.DZEvent {
  public func isMatching(criteria: DZ_Collector.DZSamplingParameterItem, matching: Swift.Dictionary<Swift.String, Swift.String>) -> Swift.Bool
}
extension DZ_Collector.DZEvent {
  public init(_ type: DZ_Collector.DZEventType)
}
extension DZ_Collector.DZEvent : Swift.Encodable {
  public func encode(to encoder: Swift.Encoder) throws
}
extension DZ_Collector.DZEvent {
  public mutating func setApplicationName(_ appName: Swift.String)
  public mutating func initWithConfigurations(config: DZ_Collector.DZConfiguration, network: DZ_Collector.DZNetworkDetails)
  public mutating func updateNetworkConfigurations(_ network: DZ_Collector.DZNetworkDetails)
  public mutating func initWithType(type: DZ_Collector.DZEventType, mediaType: DZ_Collector.DZMediaType)
}
public struct DZServerTimeOffset {
  public let epochMillis: Swift.Int?
}
extension DZ_Collector.DZServerTimeOffset : Swift.Decodable {
  public init(from decoder: Swift.Decoder) throws
}
public struct DZEventCDN {
  public var cdn: Swift.String
}
extension DZ_Collector.DZEventCDN : Swift.Encodable {
  public func encode(to encoder: Swift.Encoder) throws
}
public struct DZConfigurationEndpoints {
  public let eventIngest: Swift.String?
  public let adBlockerBait: Swift.String?
  public let serverTimeOffset: Swift.String?
  public let sampling: Swift.String?
}
extension DZ_Collector.DZConfigurationEndpoints : Swift.Decodable {
  public init(from decoder: Swift.Decoder) throws
}
public struct DZEventGeoLocation {
  public mutating func initFromConfiguration(_ net: DZ_Collector.DZNetworkDetails?)
}
extension DZ_Collector.DZEventGeoLocation {
  public func isMatching(key: Swift.String, value: Swift.String) -> Swift.Bool
}
extension DZ_Collector.DZEventGeoLocation : Swift.Encodable {
  public func encode(to encoder: Swift.Encoder) throws
}
extension DZ_Collector.DZEventGeoLocation {
  public mutating func initWithConfiguration(_ from: [Swift.String])
}
extension Swift.Float {
  public func rounded(rule: Foundation.NSDecimalNumber.RoundingMode, scale: Swift.Int) -> Swift.Float
}
@objc @_inheritsConvenienceInitializers public class DZCollector : DZ_Collector.DZBaseFramework {
  @objc override dynamic public init()
  @objc deinit
}
extension DZ_Collector.DZBaseFramework {
  public func updateNumberOfContentRequest()
  public func updateNumberOfContentPlays()
  public func updateNumberOfContentErrors()
  public func updateNumberOfAdRequest()
  public func updateNumberOfAdPlays()
  public func updateNumberOfAdErrors()
  public func getNumberOfContentRequests() -> Swift.Int
  public func getNumberOfContentPlays() -> Swift.Int
  public func getNumberOfContentErrors() -> Swift.Int
  public func getNumberOfAdRequests() -> Swift.Int
  public func getNumberOfAdPlays() -> Swift.Int
  public func getNumberOfAdErrors() -> Swift.Int
  public func getNumberOfErrors() -> Swift.Int
  public func getEngagementDuration() -> Swift.Double
  public func resetNumberOfContentRequests()
  public func resetNumberOfContentPlays()
  public func resetNumberOfContentErrors()
  public func resetNumberOfAdRequests()
  public func resetNumberOfAdPlays()
  public func resetNumberOfAdErrors()
  public func resetAll()
}
public struct DZEventCMCD {
}
extension DZ_Collector.DZEventCMCD : Swift.Encodable {
  public func encode(to encoder: Swift.Encoder) throws
}
extension DZ_Collector.DZEventCMCD {
  public mutating func initWithConfiguration(_ from: [Swift.String])
}
public struct DZEventDetailsAttributes {
}
extension DZ_Collector.DZEventDetailsAttributes : Swift.Encodable {
  public func encode(to encoder: Swift.Encoder) throws
}
extension DZ_Collector.DZEventDetailsAttributes {
  public mutating func initWithConfiguration(_ from: [Swift.String])
  public mutating func initWithType(type: DZ_Collector.DZEventType, mediaType: DZ_Collector.DZMediaType)
}
extension Foundation.URL {
  public func mimeType() -> Swift.String
  public func streamingProtocol() -> Swift.String
  public func streamingType() -> Swift.String
  public var containsImage: Swift.Bool {
    get
  }
  public var containsAudio: Swift.Bool {
    get
  }
  public var containsVideo: Swift.Bool {
    get
  }
}
public enum DZPlayerState {
  case na
  case idle
  case requested
  case buffering
  case playing
  case paused
  public var state: Swift.String {
    get
  }
}
extension DZ_Collector.DZPlayerState : Swift.Hashable {
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
extension DZ_Collector.DZPlayerState : Swift.Identifiable {
  public var id: DZ_Collector.DZPlayerState {
    get
  }
  public typealias ID = DZ_Collector.DZPlayerState
}
extension DZ_Collector.DZPlayerState : Swift.Equatable {
  public static func == (lhs: DZ_Collector.DZPlayerState, rhs: DZ_Collector.DZPlayerState) -> Swift.Bool
}
@_hasMissingDesignatedInitializers public class DZUtility {
  public static func askTrackingConsent()
  public static func getIDFA() -> Swift.String?
  public static func formatEventId(_ eCount: Swift.Int) -> Swift.String
  public static func perform(_ completion: (() -> Swift.Void)?)
  public static func perform(in seconds: Swift.Float, _ completion: (() -> Swift.Void)?)
  public static func performInBackground(_ completion: (() -> Swift.Void)?)
  public static func performInBackground(in seconds: Swift.Float, _ completion: (() -> Swift.Void)?)
  @objc deinit
}
public struct DZEventNetworkDetails {
  public var asn: Swift.String
  public var org: Swift.String
  public var isp: Swift.String
  public var mobileConnection: Swift.Bool
  public var connectionType: Swift.String
  public mutating func initFromConfiguration(_ net: DZ_Collector.DZNetworkDetails?)
}
extension DZ_Collector.DZEventNetworkDetails {
  public func isMatching(key: Swift.String, value: Swift.String) -> Swift.Bool
}
extension DZ_Collector.DZEventNetworkDetails : Swift.Encodable {
  public func encode(to encoder: Swift.Encoder) throws
}
extension DZ_Collector.DZEventNetworkDetails {
  public mutating func initWithConfiguration(_ from: [Swift.String])
}
public protocol DZFrameworkCustomMetadataProtocol {
  func getSessionCustomMetadata() -> [Swift.String : Any]?
  func setSessionCustomMetadata(_ metadata: [Swift.String : Any]?)
  func clearSessionCustomMetadata()
  func getPlayerCustomMetadata(playerContext: Swift.String) -> [Swift.String : Any]?
  func setPlayerCustomMetadata(playerContext: Swift.String, _ metadata: [Swift.String : Any]?)
  func clearPlayerCustomMetadata(playerContext: Swift.String)
}
extension DZ_Collector.DZCollector : DZ_Collector.DZFrameworkCustomMetadataProtocol {
  @objc dynamic public func getSessionCustomMetadata() -> [Swift.String : Any]?
  @objc dynamic public func setSessionCustomMetadata(_ metadata: [Swift.String : Any]?)
  @objc dynamic public func clearSessionCustomMetadata()
  @objc dynamic public func getPlayerCustomMetadata(playerContext: Swift.String) -> [Swift.String : Any]?
  @objc dynamic public func setPlayerCustomMetadata(playerContext: Swift.String, _ metadata: [Swift.String : Any]?)
  @objc dynamic public func clearPlayerCustomMetadata(playerContext: Swift.String)
}
public enum DZEventType {
  case none
  case networkTypeChange
  case networkTimed
  case datazoomLoaded
  case error
  case playerReady
  case milestone
  case heartbeat
  case volumeChange
  case mute
  case unmute
  case mediaObjectRequest
  case mediaRequest
  case bufferStart
  case bufferEnd
  case stallStart
  case stallEnd
  case mediaLoaded
  case contentLoaded
  case seekStart
  case seekEnd
  case play
  case playing
  case pause
  case resume
  case stop
  case resize
  case fullscreen
  case exitFullscreen
  case playbackReady
  case playbackStart
  case playbackComplete
  case renditionChange
  case audioTrackChanged
  case qualityChangeRequest
  case subtitleChange
  case qualifiedView
  case castStart
  case castEnd
  case playBtn
  case adSkipped
  case adBreakStart
  case adBreakEnd
  case adImpression
  case adClick
  case custom(_: Swift.String, _: [Swift.String : Any]?)
  public var key: Swift.String {
    get
  }
}
extension DZ_Collector.DZEventType : Swift.Hashable {
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
extension DZ_Collector.DZEventType : Swift.Identifiable {
  public var id: DZ_Collector.DZEventType {
    get
  }
  public typealias ID = DZ_Collector.DZEventType
}
extension DZ_Collector.DZEventType : Swift.Equatable {
  public static func == (lhs: DZ_Collector.DZEventType, rhs: DZ_Collector.DZEventType) -> Swift.Bool
}
public struct DZEventSampling {
  public var samplingRate: Swift.Double
  public var inSample: Swift.Bool
}
extension DZ_Collector.DZEventSampling : Swift.Encodable {
  public func encode(to encoder: Swift.Encoder) throws
}
public enum DZAdEventType {
  case none
  case adError
  case adBreakStart
  case adBreakEnd
  case adMediaObjectRequest
  case adRequest
  case adLoaded
  case adImpression
  case adSkipped
  case adClick
  case adPlay
  case adPlaying
  case adPause
  case adResume
  case adQualifiedView
  case adPlaybackReady
  case adPlaybackStart
  case adPlaybackComplete
  case adPlaybackCompleteAll
  case adMilestone
  case adRenditionChange
  public var key: Swift.String {
    get
  }
}
extension DZ_Collector.DZAdEventType : Swift.Hashable {
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
extension DZ_Collector.DZAdEventType : Swift.Identifiable {
  public var id: DZ_Collector.DZAdEventType {
    get
  }
  public typealias ID = DZ_Collector.DZAdEventType
}
extension DZ_Collector.DZAdEventType : Swift.Equatable {
  public static func == (lhs: DZ_Collector.DZAdEventType, rhs: DZ_Collector.DZAdEventType) -> Swift.Bool
}
extension Swift.String {
  public func base64Encode() -> Swift.String
  public func urlWithCorrelator() -> Foundation.URL
}
public struct DZService {
}
public struct DZConfigurationDatapoints {
  public let metaDataList: [Swift.String]?
  public let fluxDataList: [Swift.String]?
  public let events: DZ_Collector.DZConfigurationEvents?
  public let cmcd: [Swift.String]?
}
extension DZ_Collector.DZConfigurationDatapoints : Swift.Decodable {
  public init(from decoder: Swift.Decoder) throws
}
public struct DZSamplingParameterItem {
}
extension DZ_Collector.DZSamplingParameterItem : Swift.Encodable {
  public func encode(to encoder: Swift.Encoder) throws
}
extension DZ_Collector.DZSamplingParameterItem : Swift.Decodable {
  public init(from decoder: Swift.Decoder) throws
}
public struct DZLog {
}
public struct DZEventUserDetails {
}
extension DZ_Collector.DZEventUserDetails {
  public func isMatching(key: Swift.String, value: Swift.String) -> Swift.Bool
}
extension DZ_Collector.DZEventUserDetails : Swift.Encodable {
  public func encode(to encoder: Swift.Encoder) throws
}
extension DZ_Collector.DZEventUserDetails {
  public mutating func initWithConfiguration(_ from: [Swift.String])
}
extension Foundation.UserDefaults {
  public enum UserDefaultsKeys : Swift.String {
    case trackingAuthorization
    case trackingAuthorizationOptOut
    case appSessionID
    case appSessionViewID
    case appAdSessionID
    case appSessionStartTime
    case appSessionMovedToBackgroundTime
    public init?(rawValue: Swift.String)
    public typealias RawValue = Swift.String
    public var rawValue: Swift.String {
      get
    }
  }
  public func setTrackingAuthorization(value: Swift.Bool)
  public func isTrackingAuthorized() -> Swift.Bool
  public func isTrackingAuthorizationOptOut() -> Swift.Bool
  @objc dynamic public func createAppSessionID()
  @objc dynamic public func setAppSessionID(uuid: Swift.String)
  @objc dynamic public func clearAppSessionID()
  public func getAppSessionID() -> Swift.String
  @objc dynamic public func setAppSessionStartTime(timestamp: Swift.UInt64)
  @objc dynamic public func clearAppSessionStartTime()
  public func getAppSessionStartTime() -> Swift.UInt64
  @objc dynamic public func setAppSessionMovedToBackgroundTime(timestamp: Swift.UInt64)
  @objc dynamic public func clearAppSessionMovedToBackgroundTime()
  public func getAppSessionMovedToBackgroundTime() -> Swift.UInt64
  public func createAppSessionViewID()
  public func setAppSessionViewID(uuid: Swift.String)
  public func clearAppSessionViewID()
  public func getAppSessionViewID() -> Swift.String
}
public struct DZEventPage {
  public var dzSdkVer: Swift.String
  public var appName: Swift.String
  public init()
}
extension DZ_Collector.DZEventPage : Swift.Encodable {
  public func encode(to encoder: Swift.Encoder) throws
}
extension DZ_Collector.DZEventPage {
  public mutating func initWithConfiguration(_ from: [Swift.String])
}
public struct DZSampling {
}
extension DZ_Collector.DZSampling : Swift.Decodable {
  public init(from decoder: Swift.Decoder) throws
}
extension DZ_Collector.DZSampling : Swift.Encodable {
  public func encode(to encoder: Swift.Encoder) throws
}
public struct DZEventDevice {
  public init()
}
extension DZ_Collector.DZEventDevice {
  public func isMatching(key: Swift.String, value: Swift.String) -> Swift.Bool
}
extension DZ_Collector.DZEventDevice : Swift.Encodable {
  public func encode(to encoder: Swift.Encoder) throws
}
extension DZ_Collector.DZEventDevice {
  public mutating func initWithConfiguration(_ from: [Swift.String])
  public mutating func update()
}
public struct JSONCodingKeys : Swift.CodingKey {
  public var stringValue: Swift.String
  public var intValue: Swift.Int?
  public init?(stringValue: Swift.String)
  public init?(intValue: Swift.Int)
}
extension Swift.KeyedDecodingContainer {
  public func decode(_ type: [Swift.String : Any].Type, forKey key: K) throws -> [Swift.String : Any]
  public func decode(_ type: [[Swift.String : Any]].Type, forKey key: K) throws -> [[Swift.String : Any]]
  public func decodeIfPresent(_ type: [Swift.String : Any].Type, forKey key: K) throws -> [Swift.String : Any]?
  public func decode(_ type: [Any].Type, forKey key: K) throws -> [Any]
  public func decodeIfPresent(_ type: [Any].Type, forKey key: K) throws -> [Any]?
  public func decode(_ type: [Swift.String : Any].Type) throws -> [Swift.String : Any]
}
extension Swift.UnkeyedDecodingContainer {
  public mutating func decode(_ type: [Any].Type) throws -> [Any]
  public mutating func decode(_ type: [Swift.String : Any].Type) throws -> [Swift.String : Any]
}
extension Swift.KeyedEncodingContainer {
  public mutating func encodeIfPresent(_ value: [Swift.String : Any]?, forKey key: Swift.KeyedEncodingContainer<K>.Key) throws
  public mutating func encodeIfPresent(_ value: [Any]?, forKey key: Swift.KeyedEncodingContainer<K>.Key) throws
}
extension Swift.UnkeyedEncodingContainer {
  public mutating func encode(contentsOf sequence: [[Swift.String : Any]]) throws
  public mutating func encodeIfPresent(_ value: [Swift.String : Any]) throws
}
extension Swift.Decodable {
  public init?(dictionary: [Swift.String : Any])
}
extension Swift.Encodable {
  public var dictionary: [Swift.String : Any]? {
    get
  }
  public var prettyJSON: Swift.String {
    get
  }
}
@_hasMissingDesignatedInitializers public class JSONUtility {
  @objc deinit
}
public protocol DZSendMessageProtocol {
  func sendMessage(_ data: Foundation.Data)
  func sendMessages()
}
extension DZ_Collector.DZBaseFramework : DZ_Collector.DZSendMessageProtocol {
  public func sendMessage(_ data: Foundation.Data)
  @objc dynamic public func sendMessages()
}
extension Foundation.Data {
  public func toString() -> Swift.String
}
extension UIKit.UIImageView {
  @_Concurrency.MainActor(unsafe) public func downloadedFrom(url: Foundation.URL, contentMode mode: UIKit.UIView.ContentMode = .scaleAspectFit)
  @_Concurrency.MainActor(unsafe) public func downloadedFrom(link: Swift.String, contentMode mode: UIKit.UIView.ContentMode = .scaleAspectFit)
}
extension Foundation.Date {
  public func getTimestamp() -> Swift.UInt64
}
extension Swift.String {
  public func toData() -> Swift.String
}
public struct DZConfigurationSampling {
  public var samplingFlag: Swift.Bool?
  public var definitionNamespace: Swift.String?
  public var definitionKeyName: Swift.String?
  public var criteriaSamplingKeyName: Swift.String?
  public var criteriaMatchingFields: [Swift.String]?
}
extension DZ_Collector.DZConfigurationSampling : Swift.Decodable {
  public init(from decoder: Swift.Decoder) throws
}
@objc @_inheritsConvenienceInitializers public class DZAdEvent : ObjectiveC.NSObject {
  @objc override dynamic public init()
  @objc deinit
}
extension DZ_Collector.DZAdEvent {
  convenience public init(_ type: DZ_Collector.DZAdEventType)
  convenience public init(_ data: [Swift.String : Any])
}
public protocol DZContentSessionProtocol {
  func onCreateContentSessionID()
  func onResetContentSessionID()
}
public protocol DZPlayerCustomMetadataProtocol {
  func getPlayerCustomMetadata() -> [Swift.String : Any]?
  func setPlayerCustomMetadata(_ metadata: [Swift.String : Any]?)
  func clearPlayerCustomMetadata()
}
public protocol DZPlayerCustomEventProtocol {
  func sendCustomEvent(playerContext: Swift.String, name: Swift.String, metadata: [Swift.String : Any]?)
}
extension DZ_Collector.DZCollector : DZ_Collector.DZPlayerCustomEventProtocol {
  @objc dynamic public func sendCustomEvent(playerContext: Swift.String, name: Swift.String, metadata: [Swift.String : Any]?)
}
public struct DZEventCustomMetadata {
  public var sessionMetadata: [Swift.String : Any]
  public var playerMetadata: [Swift.String : Any]
  public var customMetadata: [Swift.String : Any]
}
extension DZ_Collector.DZEventCustomMetadata : Swift.Encodable {
  public func encode(to encoder: Swift.Encoder) throws
}
extension DZ_Collector.DZEventCustomMetadata {
  public mutating func initWithConfiguration(_ from: [Swift.String])
}
public protocol DZSessionCustomEventProtocol {
  func sendCustomEvent(name: Swift.String, metadata: [Swift.String : Any]?)
}
extension DZ_Collector.DZBaseFramework : DZ_Collector.DZSessionCustomEventProtocol {
  @objc dynamic public func sendCustomEvent(name: Swift.String, metadata: [Swift.String : Any]?)
}
public struct DZSamplingRequest {
}
extension DZ_Collector.DZSamplingRequest : Swift.Encodable {
  public func encode(to encoder: Swift.Encoder) throws
}
public struct DZSamplingDefinitionParameter {
}
extension DZ_Collector.DZSamplingDefinitionParameter : Swift.Decodable {
  public init(from decoder: Swift.Decoder) throws
}
extension DZ_Collector.DZSamplingDefinitionParameter : Swift.Encodable {
  public func encode(to encoder: Swift.Encoder) throws
}
public struct DZEventAdData {
  public var adSessionId: Swift.String
  public var adBreakId: Swift.String
  public var adId: Swift.String
  public var adPosition: Swift.String
  public var adSystem: Swift.String
  public var adWrapperSystem: [Swift.String]
  public var adDuration: Swift.Int
  public var adDealId: Swift.String
}
extension DZ_Collector.DZEventAdData : Swift.Encodable {
  public func encode(to encoder: Swift.Encoder) throws
}
extension DZ_Collector.DZEventAdData {
  public mutating func initWithConfiguration(_ from: [Swift.String])
}
extension Swift.Double {
  @inlinable public func signum() -> Swift.Double {
        if self < 0 { return -1 }
        if self > 0 { return 1 }
        return 0
    }
}
public struct DZConfiguration {
  public let configurationId: Swift.String?
  public let customerCode: Swift.String?
  public let connectorList: Swift.String?
  public let interval: Swift.Int?
  public let datapoints: DZ_Collector.DZConfigurationDatapoints?
  public let endpoints: DZ_Collector.DZConfigurationEndpoints?
  public let sampling: DZ_Collector.DZConfigurationSampling?
  public let milestoneContent: [Swift.Double]?
  public let milestoneAd: [Swift.Double]?
  public let qualifiedViewContent: [Swift.Int]?
  public let qualifiedViewAd: [Swift.Int]?
}
extension DZ_Collector.DZConfiguration : Swift.Decodable {
  public init(from decoder: Swift.Decoder) throws
}
public struct DZEventDetailsMetrics {
}
extension DZ_Collector.DZEventDetailsMetrics : Swift.Encodable {
  public func encode(to encoder: Swift.Encoder) throws
}
extension DZ_Collector.DZEventDetailsMetrics {
  public mutating func initWithConfiguration(_ from: [Swift.String])
}
public class DZBroker {
  public init(_ base: Swift.String)
  @objc deinit
  public func updateSamplingRequest()
}
extension UIKit.UIDevice {
  @_Concurrency.MainActor(unsafe) public var type: Swift.String {
    get
  }
  @_Concurrency.MainActor(unsafe) public var modelName: Swift.String {
    get
  }
  @_Concurrency.MainActor(unsafe) public var orientationString: Swift.String {
    get
  }
}
public enum ReachabilityError : Swift.Error {
  case failedToCreateWithAddress(Darwin.sockaddr, Swift.Int32)
  case failedToCreateWithHostname(Swift.String, Swift.Int32)
  case unableToSetCallback(Swift.Int32)
  case unableToSetDispatchQueue(Swift.Int32)
  case unableToGetFlags(Swift.Int32)
}
@available(*, unavailable, renamed: "Notification.Name.reachabilityChanged")
public let ReachabilityChangedNotification: Foundation.NSNotification.Name
extension Foundation.NSNotification.Name {
  public static let reachabilityChanged: Foundation.Notification.Name
}
public class Reachability {
  public typealias NetworkReachable = (DZ_Collector.Reachability) -> ()
  public typealias NetworkUnreachable = (DZ_Collector.Reachability) -> ()
  @available(*, unavailable, renamed: "Connection")
  public enum NetworkStatus : Swift.CustomStringConvertible {
    case notReachable, reachableViaWiFi, reachableViaWWAN
    public var description: Swift.String {
      get
    }
    public static func == (a: DZ_Collector.Reachability.NetworkStatus, b: DZ_Collector.Reachability.NetworkStatus) -> Swift.Bool
    public func hash(into hasher: inout Swift.Hasher)
    public var hashValue: Swift.Int {
      get
    }
  }
  public enum Connection : Swift.CustomStringConvertible {
    case unavailable, wifi, cellular
    public var description: Swift.String {
      get
    }
    @available(*, deprecated, renamed: "unavailable")
    public static let none: DZ_Collector.Reachability.Connection
    public static func == (a: DZ_Collector.Reachability.Connection, b: DZ_Collector.Reachability.Connection) -> Swift.Bool
    public func hash(into hasher: inout Swift.Hasher)
    public var hashValue: Swift.Int {
      get
    }
  }
  public var whenReachable: DZ_Collector.Reachability.NetworkReachable?
  public var whenUnreachable: DZ_Collector.Reachability.NetworkUnreachable?
  @available(*, deprecated, renamed: "allowsCellularConnection")
  final public let reachableOnWWAN: Swift.Bool
  public var allowsCellularConnection: Swift.Bool
  public var notificationCenter: Foundation.NotificationCenter
  @available(*, deprecated, renamed: "connection.description")
  public var currentReachabilityString: Swift.String {
    get
  }
  @available(*, unavailable, renamed: "connection")
  public var currentReachabilityStatus: DZ_Collector.Reachability.Connection {
    get
  }
  public var connection: DZ_Collector.Reachability.Connection {
    get
  }
  required public init(reachabilityRef: SystemConfiguration.SCNetworkReachability, queueQoS: Dispatch.DispatchQoS = .default, targetQueue: Dispatch.DispatchQueue? = nil, notificationQueue: Dispatch.DispatchQueue? = .main)
  convenience public init(hostname: Swift.String, queueQoS: Dispatch.DispatchQoS = .default, targetQueue: Dispatch.DispatchQueue? = nil, notificationQueue: Dispatch.DispatchQueue? = .main) throws
  convenience public init(queueQoS: Dispatch.DispatchQoS = .default, targetQueue: Dispatch.DispatchQueue? = nil, notificationQueue: Dispatch.DispatchQueue? = .main) throws
  @objc deinit
}
extension DZ_Collector.Reachability {
  public func startNotifier() throws
  public func stopNotifier()
  @available(*, deprecated, message: "Please use `connection != .none`")
  public var isReachable: Swift.Bool {
    get
  }
  @available(*, deprecated, message: "Please use `connection == .cellular`")
  public var isReachableViaWWAN: Swift.Bool {
    get
  }
  @available(*, deprecated, message: "Please use `connection == .wifi`")
  public var isReachableViaWiFi: Swift.Bool {
    get
  }
  public var description: Swift.String {
    get
  }
}
public enum DZMediaType {
  case none
  case content
  case ad
  public var key: Swift.String {
    get
  }
}
extension DZ_Collector.DZMediaType : Swift.Hashable {
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
extension DZ_Collector.DZMediaType : Swift.Identifiable {
  public var id: DZ_Collector.DZMediaType {
    get
  }
  public typealias ID = DZ_Collector.DZMediaType
}
extension DZ_Collector.DZMediaType : Swift.Equatable {
  public static func == (lhs: DZ_Collector.DZMediaType, rhs: DZ_Collector.DZMediaType) -> Swift.Bool
}
public struct DZEventPlayer {
}
extension DZ_Collector.DZEventPlayer {
  public func isMatching(key: Swift.String, value: Swift.String) -> Swift.Bool
}
extension DZ_Collector.DZEventPlayer : Swift.Encodable {
  public func encode(to encoder: Swift.Encoder) throws
}
extension DZ_Collector.DZEventPlayer {
  public mutating func initWithConfiguration(_ from: [Swift.String])
}
public struct DZEventOpsMetadata {
}
extension DZ_Collector.DZEventOpsMetadata : Swift.Encodable {
  public func encode(to encoder: Swift.Encoder) throws
}
extension DZ_Collector.DZEventOpsMetadata {
  public mutating func updatePlayerContextId(_ playerContextId: Swift.String)
}
public protocol DZFrameworkAdProtocol {
  func sendAdEvent(playerContext: Swift.String, event: Swift.String, adData: [Swift.String : Any]?)
}
extension DZ_Collector.DZCollector : DZ_Collector.DZFrameworkAdProtocol {
  @objc dynamic public func sendAdEvent(playerContext: Swift.String, event: Swift.String, adData: [Swift.String : Any]?)
}
public struct DZEventVideo {
  public var title: Swift.String
  public var source: Swift.String
  public var mediaType: DZ_Collector.DZMediaType
  public var duration: Swift.Float
  public var frameRate: Swift.Float
  public var playerHeight: Swift.Int
  public var playerWidth: Swift.Int
  public var assetId: Swift.String
}
extension DZ_Collector.DZEventVideo : Swift.Encodable {
  public func encode(to encoder: Swift.Encoder) throws
}
extension DZ_Collector.DZEventVideo {
  public mutating func initWithConfiguration(_ from: [Swift.String])
}
public struct DZNetworkDetails {
}
extension DZ_Collector.DZNetworkDetails : Swift.Decodable {
  public init(from decoder: Swift.Decoder) throws
}
@objc @_inheritsConvenienceInitializers public class DZBaseFramework : ObjectiveC.NSObject {
  @objc override dynamic public init()
  @objc deinit
  public static func getName() -> Swift.String
  public static func getVersion() -> Swift.String
  public static func getApplicationName() -> Swift.String
  @objc public func initialize4Dev(configurationId: Swift.String, url: Swift.String, _ completion: @escaping (Swift.Bool, Swift.Error?) -> Swift.Void)
  @objc(initializeWithConfiguration:onCompletion:) public func initialize(configurationId: Swift.String, _ completion: @escaping (Swift.Bool, Swift.Error?) -> Swift.Void)
  @objc(deinitialize) public func deinitialize()
}
public protocol DZBitmovinFrameworkProtocol {
  func initPlayer(url: Swift.String, playerView: UIKit.UIView, license: Swift.String, _ completion: ((Swift.String?, BitmovinPlayerCore.Player?, Swift.Error?) -> Swift.Void)?)
  func initPlayer(url: Swift.String, playerView: UIKit.UIView, license: Swift.String, metadata: [Swift.String : Any]?, _ completion: ((Swift.String?, BitmovinPlayerCore.Player?, Swift.Error?) -> Swift.Void)?)
  func initPlayer(player: BitmovinPlayerCore.Player, _ completion: ((Swift.String?, Swift.Error?) -> Swift.Void)?)
  func initPlayer(player: BitmovinPlayerCore.Player, metadata: [Swift.String : Any]?, _ completion: ((Swift.String?, Swift.Error?) -> Swift.Void)?)
  func deinitPlayer(playerContext: Swift.String, _ completion: ((Swift.Bool, Swift.Error?) -> Swift.Void)?)
}
extension DZ_Collector.DZCollector : DZ_Collector.DZBitmovinFrameworkProtocol {
  @objc(initPlayerWithUrl:andPlayerView:andLicense:onCompletion:) dynamic public func initPlayer(url: Swift.String, playerView: UIKit.UIView, license: Swift.String, _ completion: ((Swift.String?, BitmovinPlayerCore.Player?, Swift.Error?) -> Swift.Void)?)
  @objc(initPlayerWithUrl:andPlayerView:andLicense:andMetadata:onCompletion:) dynamic public func initPlayer(url: Swift.String, playerView: UIKit.UIView, license: Swift.String, metadata: [Swift.String : Any]?, _ completion: ((Swift.String?, BitmovinPlayerCore.Player?, Swift.Error?) -> Swift.Void)?)
  @objc(initPlayer:onCompletion:) dynamic public func initPlayer(player: BitmovinPlayerCore.Player, _ completion: ((Swift.String?, Swift.Error?) -> Swift.Void)?)
  @objc(initPlayer:andMetadata:onCompletion:) dynamic public func initPlayer(player: BitmovinPlayerCore.Player, metadata: [Swift.String : Any]?, _ completion: ((Swift.String?, Swift.Error?) -> Swift.Void)?)
  @objc(deinitPlayerWithContext:onCompletion:) dynamic public func deinitPlayer(playerContext: Swift.String, _ completion: ((Swift.Bool, Swift.Error?) -> Swift.Void)?)
}
public struct DZEventDetails {
}
extension DZ_Collector.DZEventDetails {
  public init(_ type: DZ_Collector.DZEventType)
}
extension DZ_Collector.DZEventDetails : Swift.Encodable {
  public func encode(to encoder: Swift.Encoder) throws
}
extension DZ_Collector.DZEventDetails {
  public mutating func initWithConfiguration(meta: [Swift.String], flux: [Swift.String])
  public mutating func initWithType(type: DZ_Collector.DZEventType, mediaType: DZ_Collector.DZMediaType)
}
extension Foundation.UserDefaults.UserDefaultsKeys : Swift.Equatable {}
extension Foundation.UserDefaults.UserDefaultsKeys : Swift.Hashable {}
extension Foundation.UserDefaults.UserDefaultsKeys : Swift.RawRepresentable {}
@available(*, unavailable, renamed: "Connection")
extension DZ_Collector.Reachability.NetworkStatus : Swift.Equatable {}
@available(*, unavailable, renamed: "Connection")
extension DZ_Collector.Reachability.NetworkStatus : Swift.Hashable {}
extension DZ_Collector.Reachability.Connection : Swift.Equatable {}
extension DZ_Collector.Reachability.Connection : Swift.Hashable {}
