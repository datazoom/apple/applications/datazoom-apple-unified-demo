// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.7.1 (swiftlang-5.7.1.135.3 clang-1400.0.29.51)
// swift-module-flags: -target arm64-apple-ios12.1-simulator -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -module-name DZDemoApplicationCommon
// swift-module-flags-ignorable: -enable-bare-slash-regex
import AVFoundation
@_exported import DZDemoApplicationCommon
import Foundation
import Swift
import _Concurrency
import _StringProcessing
public enum DZAdType {
  case none
  case customUrl(_: Swift.String)
  case singleInlineLinear
  case singleSkippableInline
  case singleRedirectLinear
  case singleRedirectError
  case singleRedirectBroken
  case singleVPAID20Linear
  case singleVPAID20NonLinear
  case singleNonlinearInline
  case VMAPSessionAdRulePreRoll
  case VMAPPreroll
  case VMAPPrerollBumper
  case VMAPPostroll
  case VMAPPostrollBumper
  case VMAPPreMidPostRollsSingleAds
  case VMAPPrerollSingleAdMidRollStandard3AdsPostRollSingleAd
  case VMAPPrerollSingleAdMidRollOptimized3AdsPostRollSingleAd
  case VMAPPrerollSingleAdMidRollStandard3AdsPostRollSingleAdBumpers
  case VMAPPrerollSingleAdMidRollOptimized3AdsPostRollSingleAdBumpers
  case VMAPPrerollSingleAdMidRollStandard5AdsEveryR10Seconds
  case SIMIDSurveyPreRoll
  public static let defaultType: DZDemoApplicationCommon.DZAdType
  public var title: Swift.String {
    get
  }
  public var url: Swift.String? {
    get
  }
  public var shouldEnterCustomUrl: Swift.Bool {
    get
  }
  public static let allCases: [DZDemoApplicationCommon.DZAdType]
}
extension DZDemoApplicationCommon.DZAdType : Swift.Hashable {
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
extension DZDemoApplicationCommon.DZAdType : Swift.Identifiable {
  public var id: DZDemoApplicationCommon.DZAdType {
    get
  }
  public typealias ID = DZDemoApplicationCommon.DZAdType
}
extension DZDemoApplicationCommon.DZAdType : Swift.Equatable {
  public static func == (lhs: DZDemoApplicationCommon.DZAdType, rhs: DZDemoApplicationCommon.DZAdType) -> Swift.Bool
}
extension DZDemoApplicationCommon.DZAdType : Swift.CaseIterable {
  public static func numberOfSections() -> Swift.Int
  public static func getSection(_ section: Swift.Int) -> DZDemoApplicationCommon.DZAdType
  public typealias AllCases = [DZDemoApplicationCommon.DZAdType]
}
extension Foundation.Bundle {
  public var releaseVersionNumber: Swift.String? {
    get
  }
  public var buildVersionNumber: Swift.String? {
    get
  }
  public var releaseVersionNumberPretty: Swift.String {
    get
  }
  public var buildVersionNumberPretty: Swift.String {
    get
  }
}
extension AVFoundation.AVMediaSelectionOption {
  public var languageName: Swift.String? {
    get
  }
}
extension Swift.String {
  public static let developmentURL: Swift.String
  public static let stagingURL: Swift.String
  public static let productionURL: Swift.String
}
public enum DZEnvironment : Swift.Int {
  case development
  case staging
  case production
  public var urlString: Swift.String {
    get
  }
  public static var defaultEnvironment: DZDemoApplicationCommon.DZEnvironment {
    get
  }
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
public enum DZVideoType {
  case mp4
  case mp4Short
  case mp4Random
  case mp4Multilingual
  case hls
  case hlsShort
  case hlsWithSubtitles
  case hlsMultilingual
  case live
  case liveWithSubtitle
  case liveMultilingual
  case errorWrongType
  case errorWrongUrl
  case errorUrl
  case customUrl(_: Swift.String)
  public static let defaultType: DZDemoApplicationCommon.DZVideoType
  public var title: Swift.String {
    get
  }
  public var url: Swift.String {
    get
  }
  public var shouldRequestAd: Swift.Bool {
    get
  }
  public var shouldEnterCustomUrl: Swift.Bool {
    get
  }
  public static var allCases: [DZDemoApplicationCommon.DZVideoType]
  public static var allMP4: [DZDemoApplicationCommon.DZVideoType]
  public static var allHLS: [DZDemoApplicationCommon.DZVideoType]
  public static var allLive: [DZDemoApplicationCommon.DZVideoType]
  public static var allError: [DZDemoApplicationCommon.DZVideoType]
}
extension DZDemoApplicationCommon.DZVideoType : Swift.Hashable {
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
extension DZDemoApplicationCommon.DZVideoType : Swift.Identifiable {
  public var id: DZDemoApplicationCommon.DZVideoType {
    get
  }
  public typealias ID = DZDemoApplicationCommon.DZVideoType
}
extension DZDemoApplicationCommon.DZVideoType : Swift.Equatable {
  public static func == (lhs: DZDemoApplicationCommon.DZVideoType, rhs: DZDemoApplicationCommon.DZVideoType) -> Swift.Bool
}
extension DZDemoApplicationCommon.DZVideoType : Swift.CaseIterable {
  public static func numberOfSections() -> Swift.Int
  public static func getSection(_ section: Swift.Int) -> DZDemoApplicationCommon.DZVideoType
  public typealias AllCases = [DZDemoApplicationCommon.DZVideoType]
}
extension Swift.String {
  public func toJSON() -> Any?
}
public class DZPlayerConfiguration {
  public init()
  public init(_ assetUrl: Swift.String?, _ assetType: DZDemoApplicationCommon.DZVideoType?, _ adType: DZDemoApplicationCommon.DZAdType?, _ adUrl: Swift.String?)
  @objc deinit
}
public class DZLibraryConfiguration {
  public var configurationId: Swift.String
  public var apiEndpoint: Swift.String
  public init()
  public init(_ configurationId: Swift.String, _ apiEndpoint: Swift.String)
  @objc deinit
}
extension Swift.Dictionary {
  public func jsonString(prettify: Swift.Bool = false) -> Swift.String?
}
extension DZDemoApplicationCommon.DZEnvironment : Swift.Equatable {}
extension DZDemoApplicationCommon.DZEnvironment : Swift.Hashable {}
extension DZDemoApplicationCommon.DZEnvironment : Swift.RawRepresentable {}
