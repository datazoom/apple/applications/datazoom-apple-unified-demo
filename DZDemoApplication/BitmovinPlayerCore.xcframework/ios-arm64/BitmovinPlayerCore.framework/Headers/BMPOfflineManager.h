//
// Bitmovin Player iOS SDK
// Copyright (C) 2017, Bitmovin GmbH, All Rights Reserved
//
// This source code and its use and distribution, is subject to the terms
// and conditions of the applicable license agreement.
//

#import <Foundation/Foundation.h>
#import <BitmovinPlayerCore/BMPOfflineState.h>

@protocol BMPOfflineManagerDelegate;
@protocol BMPOfflineContentManager;
@class BMPSourceConfig;
@class BMPOfflineSourceConfig;
@class BMPOfflineTrackSelection;
@class BMPOfflineConfig;
@class BMPDrmLicenseInformation;
@class BMPDownloadConfig;

NS_ASSUME_NONNULL_BEGIN

/**
 * IMPORTANT: Methods from BMPOfflineManager need to be called from the main thread.
 *
 * Do not create own instances of this class, instead use [OfflineManager sharedInstance] to obtain a reference to the singleton.
 */
NS_SWIFT_NAME(OfflineManager)
API_AVAILABLE(ios(14.0))
@interface BMPOfflineManager : NSObject
/**
 * Value in megabytes for minimum system free space available before suspending active downloads.
 * Default value is 500Mb.
 */
@property (class, nonatomic) NSUInteger minimumAvailableSpaceThreshold;
/**
 * Indicates if the OfflineManager is initialized.
 */
@property (class, readonly) BOOL isOfflineManagerInitialized;
/**
 * Specifies if an active WiFi connection is required for downloading media for offline playback.
 * Default is `NO`.
 */
@property (nonatomic, assign) BOOL restrictMediaDownloadsToWiFi;

/**
 Indicates if the `OfflineManager` has finished restoring suspended downloads.
 @warning Any `resume` or `cancel` actions triggered before this returns `true` are not guaranteed and might result in unexpected behaviour.
 */
@property (nonatomic, readonly) BOOL areSuspendedDownloadsRestored;

/**
 The delegate for the `OfflineManager`
 */
@property (nonatomic, weak) id<BMPOfflineManagerDelegate> delegate;

- (instancetype)init NS_UNAVAILABLE;
+ (instancetype)new NS_UNAVAILABLE;
/**
 * @return The singleton instance of the BMPOfflineManager.
 */
+ (instancetype)sharedInstance __TVOS_PROHIBITED;
/**
 * Has to be called in your AppDelegate's `application(application:didFinishLaunchingWithOptions:)` method to initialize
 * handling of offline content.
 *
 * If the shared instance is already initialized, this method will not have any effect.
 */
+ (void)initializeOfflineManager __TVOS_PROHIBITED;
/**
 * Has to be called in your AppDelegate's `application(application:didFinishLaunchingWithOptions:)` method to initialize
 * handling of offline content.
 *
 * Initializes the shared instance with a given `OfflineConfig`.
 * If the shared instance is already initialized, this method will not have any effect.
 *
 * @param offlineConfig The `OfflineConfig`
 */
+ (void)initializeOfflineManagerWithOfflineConfig:(BMPOfflineConfig *)offlineConfig NS_SWIFT_NAME(initializeOfflineManager(offlineConfig:)) __TVOS_PROHIBITED;
/**
 * Returns an `OfflineContentManager` instance which can be used to manage offline content and offline
 * DRM related tasks for the provided `SourceConfig`.
 *
 * The instance returned by this method will always be the same for the same `SourceConfig`.
 *
 * This method will throw an error (or return `nil` in ObjC) in case a `SourceConfig` is passed that is not supported for being downloaded.
 * A `SourceConfig` is only valid for download when it's configured using an HLS asset.
 *
 * @param sourceConfig A `SourceConfig` for which the `OfflineContentManger` is requested.
 * @return `OfflineContentManager` instance for the provided `SourceConfig`
 * @throws an error if the provided `SourceConfig` is not supported.
 */
- (nullable id<BMPOfflineContentManager>)offlineContentManagerForSourceConfig:(BMPSourceConfig *)sourceConfig
                                                                        error:(NSError **)error;
/**
 * Returns an `OfflineContentManager` instance which can be used to manage offline content and offline
 * DRM related tasks for the provided `SourceConfig`.
 *
 * The provided `identifier` will be used to create a folder containing needed resources for the offline content.
 *
 * The instance returned by this method will always be the same for the same `identifier`.
 *
 * This method will throw an error (or return `nil` in ObjC) in case a `SourceConfig` is passed that is not supported for being downloaded.
 * A `SourceConfig` is only valid for download when it's configured using an HLS asset.
 *
 * @warning If an `OfflineContentManager` is acquired using this method, subsequent calls to
 * any content related API on the `OfflineManager` are not supported, meaning
 * they could result in unexpected behaviour.
 * @param sourceConfig A `SourceConfig` for which the `OfflineContentManger` is requested.
 * @param identifier A unique identifier for the given `SourceConfig` which must not change once provided.
 * @return `OfflineContentManager` instance for the provided `SourceConfig`
 * @throws an error if the provided `SourceConfig` is not supported. 
 */
- (nullable id<BMPOfflineContentManager>)offlineContentManagerForSourceConfig:(BMPSourceConfig *)sourceConfig
                                                                   identifier:(NSString *)identifier
                                                                        error:(NSError **)error NS_SWIFT_NAME(offlineContentManager(for:id:));
/**
 * Should be called from your AppDelegate when application(application:handleEventsForBackgroundURLSession:completionHandler:)
 * is called by the system.
 *
 * @param completionHandler The completion handler which is provided by the system.
 * @param identifier The identifier which is provided by the system.
 */
- (void)addCompletionHandler:(void (^)(void))completionHandler forIdentifier:(NSString *)identifier NS_SWIFT_NAME(add(completionHandler:for:));
@end

NS_ASSUME_NONNULL_END
