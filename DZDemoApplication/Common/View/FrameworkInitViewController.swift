//
//  FrameworkInitViewController.swift
//  DZDemoApplication
//
//  Created by Ugljesha on 24.5.22..
//

import Foundation
import UIKit
import AVKit
import AVFoundation
import os

import DZDemoApplicationCommon
import DZDemoApplicationCommonIMA

#if AKAMAI
import AmpCore
#elseif BITMOVIN
import BitmovinPlayerCore
#endif

import DZ_Collector

private let subsystem = "io.datazoom.DZFramework"

#if AKAMAI
private let key = "datazoom"
#endif

struct DZDemoLog {
    static let demoapp = OSLog(subsystem: subsystem, category: "demoapp")
}

class FrameworkInitViewController: UIViewController {
    // configuration - develop / staging / production
#if NATIVE
    static private let configId: [String] = ["69d2ec1b-5dd2-499b-b257-6c26779ab29b","0fbd6039-7478-4c7e-bd51-4a676e239c3a"]
#elseif AKAMAI
    static private let configId: [String] = ["d05d561b-8040-4c89-90de-56e64b1a416a","4351d62c-9ab1-495e-9537-54ce8fa9b8af"]
#elseif BITMOVIN
    static private let configId: [String] = ["4a1aed64-0766-4329-b453-369de6385b44",""]
#endif
        
    @IBOutlet private weak var releaseVersion: UILabel!
    @IBOutlet weak var frameworkBuildVersion: UILabel!
    
    @IBOutlet private weak var environments: UISegmentedControl!
    @IBOutlet private weak var configurationIdTextField: UITextField!

    @IBOutlet private weak var initializeLibraryButton: UIButton!
    
    var libraryConfiguration : DZLibraryConfiguration = DZLibraryConfiguration()
    var sdkInitDoneFlag: Bool = false
    
    var environmentChoioce = 0 // staging
    var configurationId = ""
    var selectedEnvironmentUrl: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        releaseVersion.text = Bundle.main.releaseVersionNumberPretty

        environments.selectedSegmentIndex = environmentChoioce
        
        frameworkBuildVersion.text = "build \(DZBaseFramework.getVersion())"
        configurationId = FrameworkInitViewController.configId[environmentChoioce]
        DispatchQueue.main.async {
            self.configurationIdTextField.text = self.configurationId
            self.configurationIdTextField.isEnabled = true
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        DemoAppData.shared.framework?.deinitialize()
        DemoAppData.shared.framework = nil
        
        configurationIdTextField.isEnabled = true
        self.initializeLibraryButton.isEnabled = true
        
        DispatchQueue.main.async {
            self.configurationIdTextField.text = self.configurationId
        }
    }

}


extension FrameworkInitViewController {
    @IBAction private func pressEnvironmentSegment(_ sender: UISegmentedControl) {
        let tag:Int = sender.selectedSegmentIndex
        
        environmentChoioce = tag
        configurationId = FrameworkInitViewController.configId[environmentChoioce]
        DispatchQueue.main.async {
            self.configurationIdTextField.text = self.configurationId
            self.selectedEnvironmentUrl = DZEnvironment(rawValue: tag+1)?.urlString
        }

        libraryConfiguration.apiEndpoint = selectedEnvironmentUrl ?? ""
        if(selectedEnvironmentUrl != nil && selectedEnvironmentUrl != ""){
            self.initializeLibraryButton.isEnabled = true
        }
    }
    
    @IBAction private func initLibraryButtonPressed(sender: UIButton) {
        libraryConfiguration = DZLibraryConfiguration(configurationId, DZEnvironment(rawValue:environmentChoioce+1)?.urlString ?? "")
        
        initializeLibraryButton.isEnabled = false
        
        initLibraryWithConfiguration(libraryConfiguration) { (success, error) in
            if success == true {
                #if AKAMAI
                AmpPlayer.setApiKey(key)
                #endif
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                    DemoAppData.shared.selectedPlayerId = ""
                    DemoAppData.shared.players = [:]
                    DemoAppData.shared.customEventName = ""
                    DemoAppData.shared.customEventData = [:]
                    
                    DemoAppData.shared.players = [:]
                    
                    self.initializeLibraryButton.isEnabled = true
                    
                    self.performSegue(withIdentifier: "MainViewSegue", sender: self)
                }
            }
            else {
                DispatchQueue.main.async {
                    self.initializeLibraryButton.isEnabled = true
                }
                if let error = error {
                    os_log("Error %s", log: DZDemoLog.demoapp, type: .error, error.localizedDescription)
                    DispatchQueue.main.async {
                        let alert = UIAlertController(title: "Initialization error", message: "Configuration not found", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            }
        }
    }
}


// Initialization
extension FrameworkInitViewController {
    func validateConfigId(configId:String) -> Bool {
        if configId.range(of: "[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}", options: .regularExpression, range: nil, locale: nil) != nil {
            return true
        }else {
            return false
        }
    }
    
    func initLibraryWithConfiguration(_ libConfig: DZLibraryConfiguration, completion: @escaping (Bool,Error?) -> Void){
        DemoAppData.shared.framework = DZCollector()
        DemoAppData.shared.framework?.initialize4Dev(configurationId: libConfig.configurationId, url: libConfig.apiEndpoint, { success, error in
            if success == true {
                os_log("Init Library done", log: DZDemoLog.demoapp, type: .info)
                DemoAppData.shared.initData()
                completion(true, nil)
            }
            else {
                if let error = error {
                    os_log("Error %s", log: DZDemoLog.demoapp, type: .error, error.localizedDescription)
                }
                completion(false, error)
            }
        })
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.destination is PlayersViewController {
//            let vc = segue.destination as? MainViewController
        }
    }
}

extension FrameworkInitViewController : UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        if let newConfigurationId = textField.text {
            if validateConfigId(configId: newConfigurationId) == true {
                self.libraryConfiguration.configurationId = newConfigurationId
                self.configurationIdTextField.text = newConfigurationId
            }
            else {
                self.configurationIdTextField.text = self.libraryConfiguration.configurationId
            }
        }
    }
        
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
}
