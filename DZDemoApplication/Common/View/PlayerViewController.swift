//
//  PlayerViewController.swift
//  DZDemoApplication
//
//  Created by Ugljesha on 11.10.22..
//

import Foundation
import UIKit
import AVKit
import AVFoundation
import os

class PlayerViewController: UIViewController {
    var controller: AVPlayerViewController?
    var player: AVPlayer?

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let controller = controller {
            self.addChildController(child: controller)
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        if let controller = controller {
            self.removeChildController(child: controller)
        }
        
        super.viewDidDisappear(animated)
    }
        
    private func addChildController(child: AVPlayerViewController) {
        self.addChild(child)
        self.view.addSubview(child.view)
        
        child.view.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            child.view.centerYAnchor.constraint(equalTo: self.view.centerYAnchor),
            child.view.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
            child.view.widthAnchor.constraint(equalTo: self.view.widthAnchor),
            child.view.heightAnchor.constraint(equalTo: self.view.heightAnchor)
        ])
        child.didMove(toParent: self)
    }
    
    private func removeChildController(child: AVPlayerViewController) {
        child.willMove(toParent: nil)
        child.view.removeFromSuperview()
        child.removeFromParent()
    }
    
}
