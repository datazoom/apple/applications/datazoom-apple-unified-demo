//
//  EnterMetadataViewController.swift
//  DZNativeDemoApplication
//
//  Created by Vuk on 13.2.22..
//

import Foundation
import UIKit

class EnterMetadataViewController: UIViewController {

    var customMetadataController: CustomMetadataViewController? = nil
    
    @IBOutlet weak var enterDataTitleLabel: UILabel!
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet private weak var dataTextField: UITextField!
    
    @IBOutlet weak var okButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    
    var enterDataTitle: String? = ""

    var name:String? = ""
    var data:String? = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        enterDataTitleLabel.text = enterDataTitle
        nameTextField.text = name
        dataTextField.text = data
        if name != "" {
            nameTextField.isEnabled = false
        }
    }
        
    @IBAction private func cancelButtonPressed(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction private func okButtonPressed(_ sender: Any) {
        if name != "" && data != "" {
            customMetadataController?.onDataChanged(name: name, data: data)
            _ = navigationController?.popViewController(animated: true)
        }
    }
}


extension EnterMetadataViewController : UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
    }

    func textFieldDidChangeSelection(_ textField: UITextField) {
        if(textField == nameTextField){
            self.name = nameTextField.text
        }
        else if(textField == dataTextField){
            self.data = dataTextField.text
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
}
