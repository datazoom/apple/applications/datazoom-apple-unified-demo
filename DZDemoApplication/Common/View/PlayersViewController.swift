//
//  ViewController.swift
//  DZNativeDemoApplication
//
//  Created by Vuk on 27.11.21..
//

import Foundation
import UIKit
import AVKit
import AVFoundation
import os

import DZDemoApplicationCommon
import DZDemoApplicationCommonIMA

#if AKAMAI
import AmpCore
#elseif BITMOVIN
import BitmovinPlayerCore
#endif

import DZ_Collector


private let subsystem = "io.datazoom.DZFramework"

#if BITMOVIN
private let key = "e64af98f-9a58-47d5-a7d9-9e5fc845b332" // "9d5e04d1-31b3-42fe-a326-b10b248e2ada"
#endif

struct DZDemoPlayer {
    static let demoplayer = OSLog(subsystem: subsystem, category: "demoplayer")
}

class PlayersViewController: UIViewController {
    @IBOutlet private weak var videoTypeButton: UIButton!
    @IBOutlet private weak var videoTypeLabel: UILabel!
    
    @IBOutlet private weak var adTypeButton: UIButton!
    @IBOutlet private weak var adTypeLabel: UILabel!
    
    #if os(iOS)
    @IBOutlet private weak var loopSwitch: UISwitch!
    #elseif os(tvOS)
    @IBOutlet private weak var loopButton: UIButton!
    #endif
    
    @IBOutlet private weak var setPlayerMetadataButton: UIButton!
    @IBOutlet private weak var setSessionMetadataButton: UIButton!
    
    @IBOutlet private weak var resetPlayerMetadataButton: UIButton!
    @IBOutlet private weak var resetSessionMetadataButton: UIButton!
    
    @IBOutlet weak var createCustomEventButton: UIButton!
    @IBOutlet weak var sendCustomEventButton: UIButton!
    
    @IBOutlet private weak var playUrlButton: UIButton!
    @IBOutlet private weak var playPlayerButton: UIButton!
    
    @IBOutlet weak var removePlayerButton: UIButton!
    @IBOutlet weak var fullScreenButton: UIButton!
    
    @IBOutlet weak var playerContainerView: UIView!
    
    @IBOutlet private weak var playersTableView : UITableView!
    
    static let adsDelay: Int = 100
    
    var selectedVideoType: DZVideoType = DZVideoType.defaultType
    var selectedAdType: DZAdType = DZAdType.defaultType
    var selectedVideoUrl: String?
    var selectedAdUrl: String?
    
    var playerLooper: AVPlayerLooper?
    var queuePlayer: AVQueuePlayer?
    var loopVideo: Bool = false
    
    var customMetadataSelector: Int = 0
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        selectedVideoUrl = selectedVideoType.url
        videoTypeLabel.text = selectedVideoType.title
        
        selectedAdUrl = selectedAdType.url
        adTypeLabel.text = selectedAdType.title
        
        loopVideo = false
        
        #if os(iOS)
        self.loopSwitch.setOn(false, animated: false)
        
        removePlayerButton.setTitle("", for: UIControl.State.normal)
        fullScreenButton.setTitle("", for: UIControl.State.normal)
        #elseif os(tvOS)
        self.loopButton.setTitle("", for: UIControl.State.normal)
        #endif
        enableControlls(flag: false)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let enableFlag = DemoAppData.shared.selectedPlayerId != nil && DemoAppData.shared.selectedPlayerId != ""
        
        enableControlls(flag: enableFlag)
        
        if DemoAppData.shared.fullscreenFlag {
            if let selected = DemoAppData.shared.selectedPlayerId, let controller = DemoAppData.shared.controllers[selected] {
                DemoAppData.shared.fullscreenFlag = false
                DispatchQueue.main.async {
                    self.addChildController(child: controller)
                }
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if !DemoAppData.shared.fullscreenFlag && !DemoAppData.shared.displayModelsFlag {
            if self.children.count > 0 {
                let viewControllers:[UIViewController] = self.children
                for viewContoller in viewControllers{
                    viewContoller.willMove(toParent: nil)
                    viewContoller.view.removeFromSuperview()
                    viewContoller.removeFromParent()
                }
            }
            
            DemoAppData.shared.clearData()
        }
        
        super.viewWillDisappear(animated)
    }
    
    private func addChildController(child: AVPlayerViewController) {
        self.addChild(child)
        self.playerContainerView.addSubview(child.view)
        
        child.view.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            child.view.centerYAnchor.constraint(equalTo: self.playerContainerView.centerYAnchor),
            child.view.centerXAnchor.constraint(equalTo: self.playerContainerView.centerXAnchor),
            child.view.widthAnchor.constraint(equalTo: self.playerContainerView.widthAnchor),
            child.view.heightAnchor.constraint(equalTo: self.playerContainerView.heightAnchor)
        ])
        child.didMove(toParent: self)
    }
    
    private func removeChildController(child: AVPlayerViewController) {
        child.willMove(toParent: nil)
        child.view.removeFromSuperview()
        child.removeFromParent()
//        child.view.translatesAutoresizingMaskIntoConstraints = true
    }
    
    private func ateachChildController(child: AVPlayerViewController) {
        self.playerContainerView.addSubview(child.view)
        
        child.view.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            child.view.centerYAnchor.constraint(equalTo: self.playerContainerView.centerYAnchor),
            child.view.centerXAnchor.constraint(equalTo: self.playerContainerView.centerXAnchor),
            child.view.widthAnchor.constraint(equalTo: self.playerContainerView.widthAnchor),
            child.view.heightAnchor.constraint(equalTo: self.playerContainerView.heightAnchor)
        ])
        
        child.didMove(toParent: self)
    }
    
    private func deteachChildController(child: AVPlayerViewController) {
        child.willMove(toParent: nil)
        child.view.removeFromSuperview()
        child.view.translatesAutoresizingMaskIntoConstraints = true
    }
    
    private func switchChildController(current: AVPlayerViewController?, next: AVPlayerViewController?) {
        if let ct = current {
            deteachChildController(child: ct)
        }
        if let nt = next {
            ateachChildController(child: nt)
        }
    }
    

    func enableControlls(flag: Bool){
        DispatchQueue.main.async {
//            self.setPlayerMetadataButton.isEnabled = flag
//            self.resetPlayerMetadataButton.isEnabled = flag
            
            self.removePlayerButton.isEnabled = flag
            self.fullScreenButton.isEnabled = flag
            
            self.sendCustomEventButton.isEnabled = DemoAppData.shared.customEventName != ""
        }
    }

    private func configureLoopPlayer(child: AVPlayerViewController) {
    }
}

extension PlayersViewController {
    func configureLoopPlayer(with url: URL) -> AVPlayer {
        let asset = AVAsset(url: url )
        let playerItem = AVPlayerItem(asset: asset)
        let queuePlayer = AVQueuePlayer(playerItem: playerItem)
        self.queuePlayer = queuePlayer
        self.playerLooper = AVPlayerLooper(player: queuePlayer, templateItem: playerItem)
        print(queuePlayer.items())
        return queuePlayer
    }
}

extension PlayersViewController {
    #if os(iOS)
    @IBAction private func onLoopSwitchChanged(_ sender: Any) {
        if self.loopSwitch.isOn {
            loopVideo = true
        } else {
            loopVideo = false
        }
    }
    #elseif os(tvOS)
    @IBAction private func onLoopButtonPressed(sender: UIButton) {
        loopVideo = !loopVideo
        
        if loopVideo {
            loopButton.backgroundColor = UIColor.red
        }
        else {
            loopButton.backgroundColor = UIColor.black.withAlphaComponent(0.0)
        }
    }
    #endif
    
    func onVideoTypeChanged(type: DZVideoType) {
        self.selectedVideoType = type
        self.selectedVideoUrl = type.url
        self.videoTypeLabel.text = type.title
    }
    
    func onAdTypeChanged(type: DZAdType) {
        self.selectedAdType = type
        self.selectedAdUrl = type.url
        self.adTypeLabel.text = type.title
    }
    
    func onCustomMetadataChanged(metadata: [String:Any]?) {
        if customMetadataSelector == 1 {
            let selected = DemoAppData.shared.selectedPlayerId
            if selected != nil && selected != "" {
                DemoAppData.shared.framework?.setPlayerCustomMetadata(playerContext: selected!, metadata)
            }
            else {
                DemoAppData.shared.playerCutomMetadata  = metadata
            }
        }
        else if customMetadataSelector == 2 {
            DemoAppData.shared.framework?.setSessionCustomMetadata(metadata)
        }
    }
        
    func onCustomEventChanged(eventName: String?, eventData: [String: Any]?){
        DemoAppData.shared.customEventName = eventName ?? ""
        DemoAppData.shared.customEventData = eventData ?? [:]
    }
}


extension PlayersViewController {
    func initAds(url: String, playerContext: String, contentDuration: Float = 0, _ completion: ((Bool, Error?) -> Void)?) {
        if let controller = DemoAppData.shared.controllers[playerContext] {
            let ad: DZAdDataIMA = DZAdDataIMA(playerContext: playerContext)
            ad.delegate = self
            DemoAppData.shared.ads?[playerContext] = ad
            if let ads = DemoAppData.shared.ads?[playerContext] {
                ads.requestAds(url: url, controller: controller, contentDuration: contentDuration)
                completion?(true, nil)
            }
            completion?(true, nil)
        }
        else {
            completion?(true, nil)
        }
    }
}

extension PlayersViewController: PlayersViewControllerAdProtocol {
    func sendAdEvent(playerContext: String, event: String, adData: [String : Any]?) {
        DemoAppData.shared.framework?.sendAdEvent(playerContext: playerContext, event: event, adData: adData)
    }
}

//
extension PlayersViewController {
    @IBAction private func videoTypeButtonPressed(sender: UIButton) {
        performSegue(withIdentifier: "ChooseVideoTypeSegue", sender: nil)
    }

    @IBAction private func adTypeButtonPressed(sender: UIButton) {
        performSegue(withIdentifier: "ChooseAdTypeSegue", sender: nil)
    }

    
    
    @IBAction private func setSessionCustomMetadataButtonPressed(sender: UIButton) {
        customMetadataSelector = 2
        performSegue(withIdentifier: "EnterCustomMetadataSeque", sender: nil)
    }

    @IBAction private func resetSessionCustomMetadataButtonPressed(sender: UIButton) {
        DemoAppData.shared.framework?.setSessionCustomMetadata([:])
    }
    
    
    @IBAction private func setPlayerCustomMetadataButtonPressed(sender: UIButton) {
        customMetadataSelector = 1
        performSegue(withIdentifier: "EnterCustomMetadataSeque", sender: nil)
    }
    
    @IBAction private func resetPlayerCustomMetadataButtonPressed(sender: UIButton) {
        guard let selected = DemoAppData.shared.selectedPlayerId else {
            return
        }
        DemoAppData.shared.framework?.setPlayerCustomMetadata(playerContext: selected, [:])
    }
    
    
    @IBAction private func createCustomEventButtonPressed(sender: UIButton) {
        performSegue(withIdentifier: "EnterCustomEventSeque", sender: nil)
    }
    
    @IBAction private func sendCustomEventButtonPressed(sender: UIButton) {
        let name = DemoAppData.shared.customEventName
        let data = DemoAppData.shared.customEventData
        if DemoAppData.shared.selectedPlayerId == "" || DemoAppData.shared.selectedPlayerId == nil {
            DemoAppData.shared.framework?.sendCustomEvent(name: name, metadata: data)
        }
        else {
            if let selected = DemoAppData.shared.selectedPlayerId {
                DemoAppData.shared.framework?.sendCustomEvent(playerContext: selected, name: name, metadata: data)
            }
        }
    }
    
    
    
    @IBAction private func playUrlButtonPressed(sender: UIButton) {
        guard let videoUrl = selectedVideoUrl else {
            return
        }

        if DemoAppData.shared.selectedPlayerId == "" {
            // new player
            let playerMetadata: [String:Any]? = DemoAppData.shared.playerCutomMetadata

            let controller = AVPlayerViewController()
            
            if loopVideo {
#if NATIVE
                let url = NSURL(string: videoUrl)! as URL
                let asset = AVAsset(url: url)
                let playerItem = AVPlayerItem(asset: asset)
                let queuePlayer = AVQueuePlayer(playerItem: playerItem)
                self.queuePlayer = queuePlayer
                self.playerLooper = AVPlayerLooper(player: queuePlayer, templateItem: playerItem)
                DemoAppData.shared.framework?.initPlayerLooping(player: queuePlayer, { playerId, error in
                    if let playerId = playerId {
                        DemoAppData.shared.players[playerId] = queuePlayer
                        DemoAppData.shared.controllers[playerId] = controller
                        
                        DispatchQueue.main.async {
                            controller.player = queuePlayer
                            self.addChildController(child: controller)
                            self.playersTableView.reloadData()

                            controller.player?.play()
                            
                            let enableFlag = DemoAppData.shared.selectedPlayerId != nil && DemoAppData.shared.selectedPlayerId != ""
                            self.enableControlls(flag: enableFlag)
                        }
                    }
                    else {
                        let errorMessage = error?.localizedDescription ?? "Error"
                        os_log("Init Player failed %s", log: DZDemoPlayer.demoplayer, type: .debug, errorMessage)
                    }
                })
#elseif AKAMAI
                //TODO
                // looping on Akamai
#elseif BITMOVIN
                //TODO
                // looping on Bitmovin
#endif
            }
            else {
                if (playerMetadata == nil || playerMetadata?.count == 0) {
#if NATIVE
                    DemoAppData.shared.framework?.initPlayer(url: videoUrl, playerView: playerContainerView, { playerId, player, error in
                        DemoAppData.shared.playerCutomMetadata = [:]
                        
                        if let playerId = playerId, let player = player {
                            DemoAppData.shared.players[playerId] = player
                            DemoAppData.shared.controllers[playerId] = controller

                            #if NATIVE
                            controller.player = player
                            #elseif AKAMAI
                            controller.player = player.player
                            #endif

                            DispatchQueue.main.async {
                                self.addChildController(child: controller)
                                self.playersTableView.reloadData()
                            }
                            
                            if let adUrl = self.selectedAdUrl {
                                self.prepareViewControllerForAds(controller: controller)
                                
                                let deadline = DispatchTime.now() + DispatchTimeInterval.milliseconds(PlayersViewController.adsDelay)
                                DispatchQueue.main.asyncAfter(deadline: deadline){
                                    self.initAds(url: adUrl, playerContext: playerId) { success, error in
                                        if success {
                                            os_log("Init Ads", log: DZDemoPlayer.demoplayer, type: .debug)
                                        }
                                        else {
                                            os_log("Init Ads failed", log: DZDemoPlayer.demoplayer, type: .debug)
                                        }
                                        
                                        player.play()
                                        
                                        let enableFlag = DemoAppData.shared.selectedPlayerId != nil && DemoAppData.shared.selectedPlayerId != ""
                                        self.enableControlls(flag: enableFlag)
                                    }
                                }
                            }
                            else {
                                player.play()
                                
                                let enableFlag = DemoAppData.shared.selectedPlayerId != nil && DemoAppData.shared.selectedPlayerId != ""
                                self.enableControlls(flag: enableFlag)
                            }
                        }
                    })
                     
#elseif AKAMAI
                    #if os(iOS)
                    DemoAppData.shared.framework?.initPlayer(url: videoUrl, playerView: playerContainerView, { playerId, player, error in
                        if let playerId = playerId, let player = player {
                            controller.player = player.player
                            
                            DemoAppData.shared.players[playerId] = player
                            DemoAppData.shared.controllers[playerId] = controller
                            
                            DispatchQueue.main.async {
                                self.addChildController(child: controller)
                                self.playersTableView.reloadData()
                            }
                            
                            if let adUrl = self.selectedAdUrl {
                                let deadline = DispatchTime.now() + DispatchTimeInterval.milliseconds(PlayersViewController.adsDelay)
                                DispatchQueue.main.asyncAfter(deadline: deadline){
                                    self.initAds(url: adUrl, playerContext: playerId) { success, error in
                                        if success {
                                            os_log("Init Ads", log: DZDemoPlayer.demoplayer, type: .debug)
                                        }
                                        else {
                                            os_log("Init Ads failed", log: DZDemoPlayer.demoplayer, type: .debug)
                                        }

                                        player.play()

                                        let enableFlag = DemoAppData.shared.selectedPlayerId != nil && DemoAppData.shared.selectedPlayerId != ""
                                        self.enableControlls(flag: enableFlag)
                                    }
                                }
                            }
                            else {
                                player.play()

                                let enableFlag = DemoAppData.shared.selectedPlayerId != nil && DemoAppData.shared.selectedPlayerId != ""
                                self.enableControlls(flag: enableFlag)
                            }
                        }
                    })
                    #elseif os(tvOS)
                    DemoAppData.shared.framework?.initPlayer(url: videoUrl, parentViewController: self, playerController: controller, playerView: playerContainerView, { playerId, player, error in
                        if let playerId = playerId, let player = player {
                            controller.player = player.player
                            
                            DemoAppData.shared.players[playerId] = player
                            DemoAppData.shared.controllers[playerId] = controller
                            
                            DispatchQueue.main.async {
                                self.addChildController(child: controller)
                                self.playersTableView.reloadData()
                            }
                            
                            if let adUrl = self.selectedAdUrl {
                                let deadline = DispatchTime.now() + DispatchTimeInterval.milliseconds(PlayersViewController.adsDelay)
                                DispatchQueue.main.asyncAfter(deadline: deadline){
                                    self.initAds(url: adUrl, playerContext: playerId) { success, error in
                                        if success {
                                            os_log("Init Ads", log: DZDemoPlayer.demoplayer, type: .debug)
                                        }
                                        else {
                                            os_log("Init Ads failed", log: DZDemoPlayer.demoplayer, type: .debug)
                                        }
                                        
                                        player.play()
                                        
                                        let enableFlag = DemoAppData.shared.selectedPlayerId != nil && DemoAppData.shared.selectedPlayerId != ""
                                        self.enableControlls(flag: enableFlag)
                                    }
                                }
                            }
                            else {
                                player.play()
                                
                                let enableFlag = DemoAppData.shared.selectedPlayerId != nil && DemoAppData.shared.selectedPlayerId != ""
                                self.enableControlls(flag: enableFlag)
                            }
                        }
                    })
                    #endif
#elseif BITMOVIN
                    if #available(iOS 14.0, *, tvOS 14.0, *) {
                        DemoAppData.shared.framework?.initPlayer(url: videoUrl, playerView: playerContainerView, license: key, { playerId, player, error in
                            if let playerId = playerId, let player = player {
                                DemoAppData.shared.players[playerId] = player
                                DemoAppData.shared.controllers[playerId] = controller
                                
                                let playerView = PlayerView(player: player, frame: .zero)
                                DemoAppData.shared.playerViews[playerId] = playerView

                                DispatchQueue.main.async {
                                    playerView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
                                    playerView.frame = self.playerContainerView.bounds

                                    self.playerContainerView.addSubview(playerView)
                                    self.playerContainerView.bringSubviewToFront(playerView)

                                    self.playersTableView.reloadData()
                                }

                                player.play()
                                
                                let enableFlag = DemoAppData.shared.selectedPlayerId != nil && DemoAppData.shared.selectedPlayerId != ""
                                self.enableControlls(flag: enableFlag)
                            }
                        })
                    }
#endif
                }
                else {
#if NATIVE
                    DemoAppData.shared.framework?.initPlayer(url: videoUrl, playerView: playerContainerView, metadata: playerMetadata, { playerId, player, error in
                        if let playerId = playerId, let player = player {
                            DemoAppData.shared.players[playerId] = player
                            DemoAppData.shared.controllers[playerId] = controller
                            
                            #if NATIVE
                            controller.player = player
                            #elseif AKAMAI
                            controller.player = player.player
                            #endif
                            
                            DispatchQueue.main.async {
                                self.addChildController(child: controller)
                                self.playersTableView.reloadData()
                            }
                            
                            if let adUrl = self.selectedAdUrl {
                                let deadline = DispatchTime.now() + DispatchTimeInterval.milliseconds(PlayersViewController.adsDelay)
                                DispatchQueue.main.asyncAfter(deadline: deadline){
                                    self.initAds(url: adUrl, playerContext: playerId) { success, error in
                                        if success {
                                            os_log("Init Ads", log: DZDemoPlayer.demoplayer, type: .debug)
                                        }
                                        else {
                                            os_log("Init Ads failed", log: DZDemoPlayer.demoplayer, type: .debug)
                                        }
                                        
                                        player.play()
                                        
                                        let enableFlag = DemoAppData.shared.selectedPlayerId != nil && DemoAppData.shared.selectedPlayerId != ""
                                        self.enableControlls(flag: enableFlag)
                                    }
                                }
                            }
                            else {
                                player.play()
                                
                                let enableFlag = DemoAppData.shared.selectedPlayerId != nil && DemoAppData.shared.selectedPlayerId != ""
                                self.enableControlls(flag: enableFlag)
                            }
                        }
                    })
                    
#elseif AKAMAI
                    #if os(iOS)
                    DemoAppData.shared.framework?.initPlayer(url: videoUrl, playerView: playerContainerView, metadata: playerMetadata, { playerId, player, error in
                        if let playerId = playerId, let player = player {
                            controller.player = player.player
                            
                            DemoAppData.shared.players[playerId] = player
                            DemoAppData.shared.controllers[playerId] = controller
                            
                            DispatchQueue.main.async {
                                self.addChildController(child: controller)
                                self.playersTableView.reloadData()
                            }
                            
                            if let adUrl = self.selectedAdUrl {
                                let deadline = DispatchTime.now() + DispatchTimeInterval.milliseconds(PlayersViewController.adsDelay)
                                DispatchQueue.main.asyncAfter(deadline: deadline){
                                    self.initAds(url: adUrl, playerContext: playerId) { success, error in
                                        if success {
                                            os_log("Init Ads", log: DZDemoPlayer.demoplayer, type: .debug)
                                        }
                                        else {
                                            os_log("Init Ads failed", log: DZDemoPlayer.demoplayer, type: .debug)
                                        }

                                        player.play()

                                        let enableFlag = DemoAppData.shared.selectedPlayerId != nil && DemoAppData.shared.selectedPlayerId != ""
                                        self.enableControlls(flag: enableFlag)
                                    }
                                }
                            }
                            else {
                                player.play()

                                let enableFlag = DemoAppData.shared.selectedPlayerId != nil && DemoAppData.shared.selectedPlayerId != ""
                                self.enableControlls(flag: enableFlag)
                            }
                        }
                    })
                    #elseif os(tvOS)
                    DemoAppData.shared.framework?.initPlayer(url: videoUrl, parentViewController: self, playerController: controller, playerView: playerContainerView, metadata: playerMetadata, { playerId, player, error in
                        if let playerId = playerId, let player = player {
                            controller.player = player.player
                            
                            DemoAppData.shared.players[playerId] = player
                            DemoAppData.shared.controllers[playerId] = controller
                            
                            DispatchQueue.main.async {
                                self.addChildController(child: controller)
                                self.playersTableView.reloadData()
                            }
                            
                            if let adUrl = self.selectedAdUrl {
                                let deadline = DispatchTime.now() + DispatchTimeInterval.milliseconds(PlayersViewController.adsDelay)
                                DispatchQueue.main.asyncAfter(deadline: deadline){
                                    self.initAds(url: adUrl, playerContext: playerId) { success, error in
                                        if success {
                                            os_log("Init Ads", log: DZDemoPlayer.demoplayer, type: .debug)
                                        }
                                        else {
                                            os_log("Init Ads failed", log: DZDemoPlayer.demoplayer, type: .debug)
                                        }

                                        player.play()

                                        let enableFlag = DemoAppData.shared.selectedPlayerId != nil && DemoAppData.shared.selectedPlayerId != ""
                                        self.enableControlls(flag: enableFlag)
                                    }
                                }
                            }
                            else {
                                player.play()

                                let enableFlag = DemoAppData.shared.selectedPlayerId != nil && DemoAppData.shared.selectedPlayerId != ""
                                self.enableControlls(flag: enableFlag)
                            }
                        }
                    })
                    #endif
#elseif BITMOVIN
                    DemoAppData.shared.framework?.initPlayer(url: videoUrl, playerView: playerContainerView, license: key, metadata: playerMetadata, { playerId, player, error in
                        if let playerId = playerId, let player = player {
                            DemoAppData.shared.players[playerId] = player
                            
                            let playerView = PlayerView(player: player, frame: .zero)
                            DemoAppData.shared.playerViews[playerId] = playerView
                            
                            DispatchQueue.main.async {
                                playerView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
                                playerView.frame = self.playerContainerView.bounds
                                
                                self.playerContainerView.addSubview(playerView)
                                self.playerContainerView.bringSubviewToFront(playerView)
                            
                                self.playersTableView.reloadData()
                            }
                            
                            player.play()
                            
                            let enableFlag = DemoAppData.shared.selectedPlayerId != nil && DemoAppData.shared.selectedPlayerId != ""
                            self.enableControlls(flag: enableFlag)
                        }
                    })
#endif
                }
            }

        }
        else {
            // change video in existing player
            if let selectedPlayerId = DemoAppData.shared.selectedPlayerId, let selectedPlayer = DemoAppData.shared.players[selectedPlayerId], let selectedController = DemoAppData.shared.controllers[selectedPlayerId]  {
                #if NATIVE
                let newVideoUrl = NSURL(string: videoUrl)! as URL

                let asset = AVAsset(url: newVideoUrl)
                let newPlayerItem = AVPlayerItem(asset: asset)
                
                selectedPlayer.replaceCurrentItem(with: newPlayerItem)
                selectedController.player?.play()

                let enableFlag = DemoAppData.shared.selectedPlayerId != nil && DemoAppData.shared.selectedPlayerId != ""
                self.enableControlls(flag: enableFlag)
                
                #elseif AKAMAI
                selectedPlayer.play(url: videoUrl)
                selectedPlayer.play()
                
                #elseif BITMOVIN
                
                #endif
            }
        }
    }
    
    @IBAction private func playPlayerButtonPressed(sender: UIButton) {
        guard let videoUrl = selectedVideoUrl else {
            return
        }
        
        if DemoAppData.shared.selectedPlayerId == "" {
            let playerMetadata: [String:Any]? = DemoAppData.shared.playerCutomMetadata

            let controller = AVPlayerViewController()
            
#if NATIVE
            let url = NSURL(string: videoUrl)! as URL
            let player = AVPlayer(url: url)
            
            #if os(iOS)
            if #available(iOS 13.0, *) {
                controller.showsTimecodes = true
            }
            #endif
            
#elseif AKAMAI
            #if os(iOS)
            let player = AmpPlayer(parentView: playerContainerView)
            #elseif os(tvOS)
            let player = AmpPlayer(parentViewController: self, playerController: controller, view: playerContainerView)
            #endif
            player.play(url: videoUrl)
            
#elseif BITMOVIN
            let playerConfig = PlayerConfig()
            playerConfig.key = key
            
            if let _ = self.selectedAdUrl {
                let adTag1 =  "https://pubads.g.doubleclick.net/gampad/ads?sz=640x480&iu=/124319096/external/ad_rule_samples&ciu_szs=300x250&ad_rule=1&impl=s&gdfp_req=1&env=vp&output=vmap&unviewed_position_start=1&cust_params=deployment%3Ddevsite%26sample_ar%3Dpreonly&cmsid=496&vid=short_onecue&correlator=".urlWithCorrelator()
                let adTag2 =  "https://pubads.g.doubleclick.net/gampad/ads?sz=640x480&iu=/124319096/external/single_ad_samples&ciu_szs=300x250&impl=s&gdfp_req=1&env=vp&output=vast&unviewed_position_start=1&cust_params=deployment%3Ddevsite%26sample_ct%3Dlinear&correlator=".urlWithCorrelator()
                let adTag3 =  "https://pubads.g.doubleclick.net/gampad/ads?sz=640x480&iu=/124319096/external/ad_rule_samples&ciu_szs=300x250&ad_rule=1&impl=s&gdfp_req=1&env=vp&output=vmap&unviewed_position_start=1&cust_params=deployment%3Ddevsite%26sample_ar%3Dpostonly&cmsid=496&vid=short_onecue&correlator=".urlWithCorrelator()
                
                let adSource1 = AdSource(tag: adTag1, ofType: .ima)
                let adSource2 = AdSource(tag: adTag2, ofType: .ima)
                let adSource3 = AdSource(tag: adTag3, ofType: .ima)
                
                let preRoll = AdItem(adSources: [adSource1], atPosition: "pre")
                let midRoll = AdItem(adSources: [adSource2], atPosition: "20%")
                let postRoll = AdItem(adSources: [adSource3], atPosition: "post")
                let adConfig = AdvertisingConfig(schedule: [preRoll, midRoll, postRoll])
                playerConfig.advertisingConfig = adConfig
            }

            let player = PlayerFactory.create(playerConfig: playerConfig)
            
            let url = NSURL(string: videoUrl)! as URL
            let streamingProtocol = url.streamingProtocol()
            var sourceType = SourceType.none
            switch streamingProtocol {
            case "MP4":
                sourceType = SourceType.progressive
                
            case "HLS":
                sourceType = SourceType.hls
                
            default:
                sourceType = SourceType.none
            }
                        
            let sourceConfig = SourceConfig(url: url, type: sourceType)

#endif
            
            if (playerMetadata == nil || playerMetadata?.count == 0) {
#if NATIVE
                DemoAppData.shared.framework?.initPlayer(player: player, { playerId, error in
                    if let playerId = playerId {
                        DemoAppData.shared.players[playerId] = player
                        DemoAppData.shared.controllers[playerId] = controller
                        
                        DispatchQueue.main.async {
                            controller.player = player
                            self.addChildController(child: controller)
                            self.playersTableView.reloadData()
                        }
                        if let adUrl = self.selectedAdUrl {
                            self.prepareViewControllerForAds(controller: controller)
                            
                            let deadline = DispatchTime.now() + DispatchTimeInterval.milliseconds(PlayersViewController.adsDelay)
                            DispatchQueue.main.asyncAfter(deadline: deadline){
                                self.initAds(url: adUrl, playerContext: playerId) { success, error in
                                    if success {
                                        os_log("Init Ads", log: DZDemoPlayer.demoplayer, type: .debug)
                                    }
                                    else {
                                        os_log("Init Ads failed", log: DZDemoPlayer.demoplayer, type: .debug)
                                    }
                                    
                                    controller.player?.play()
                                    
                                    let enableFlag = DemoAppData.shared.selectedPlayerId != nil && DemoAppData.shared.selectedPlayerId != ""
                                    self.enableControlls(flag: enableFlag)
                                }
                            }
                        }
                        else {
                            controller.player?.play()
                            
                            let enableFlag = DemoAppData.shared.selectedPlayerId != nil && DemoAppData.shared.selectedPlayerId != ""
                            self.enableControlls(flag: enableFlag)
                        }
                    }
                    else {
                        let errorMessage = error?.localizedDescription ?? "Error"
                        os_log("Init Player failed %s", log: DZDemoPlayer.demoplayer, type: .debug, errorMessage)
                    }
                })
                
#elseif AKAMAI
                DemoAppData.shared.framework?.initPlayer(player: player, playerView: playerContainerView, { playerId, error in
                    if let playerId = playerId {
                        DemoAppData.shared.players[playerId] = player
                        DemoAppData.shared.controllers[playerId] = controller
                        
                        controller.player = player.player
                        
                        DispatchQueue.main.async {
                            self.addChildController(child: controller)
                            self.playersTableView.reloadData()
                        }
                        
                        if let adUrl = self.selectedAdUrl {
                            self.prepareViewControllerForAds(controller: controller)

                            let deadline = DispatchTime.now() + DispatchTimeInterval.milliseconds(PlayersViewController.adsDelay)
                            DispatchQueue.main.asyncAfter(deadline: deadline){
                                self.initAds(url: adUrl, playerContext: playerId) { success, error in
                                    if success {
                                        os_log("Init Ads", log: DZDemoPlayer.demoplayer, type: .debug)
                                    }
                                    else {
                                        os_log("Init Ads failed", log: DZDemoPlayer.demoplayer, type: .debug)
                                    }
                                    
                                    player.play(url: videoUrl)
                                    
                                    let enableFlag = DemoAppData.shared.selectedPlayerId != nil && DemoAppData.shared.selectedPlayerId != ""
                                    self.enableControlls(flag: enableFlag)
                                }
                            }
                        }
                        else {
                            player.play(url: videoUrl)
                            
                            let enableFlag = DemoAppData.shared.selectedPlayerId != nil && DemoAppData.shared.selectedPlayerId != ""
                            self.enableControlls(flag: enableFlag)
                        }
                            
                    }
                })
                
#elseif BITMOVIN
                player.load(sourceConfig: sourceConfig)
                
                DemoAppData.shared.framework?.initPlayer(player: player, { playerId, error in
                    if let playerId = playerId {
                        DemoAppData.shared.players[playerId] = player
                        DemoAppData.shared.controllers[playerId] = controller
                        
                        let playerView = PlayerView(player: player, frame: .zero)
                        DemoAppData.shared.playerViews[playerId] = playerView
                        
                        DispatchQueue.main.async {
                            playerView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
                            playerView.frame = self.playerContainerView.bounds
                            
                            self.playerContainerView.addSubview(playerView)
                            self.playerContainerView.bringSubviewToFront(playerView)
                        
                            self.playersTableView.reloadData()
                        }

                        player.play()
                        
                        let enableFlag = DemoAppData.shared.selectedPlayerId != nil && DemoAppData.shared.selectedPlayerId != ""
                        self.enableControlls(flag: enableFlag)
                    }
                })
#endif
            }
            else {
#if NATIVE
                DemoAppData.shared.framework?.initPlayer(player: player, metadata: playerMetadata, { playerId, error in
                    if let playerId = playerId {
                        DemoAppData.shared.players[playerId] = player
                        DemoAppData.shared.controllers[playerId] = controller
                        
                        DispatchQueue.main.async {
                            controller.player = player
                            self.addChildController(child: controller)
                            self.playersTableView.reloadData()
                        }
                        
                        if let adUrl = self.selectedAdUrl {
                            self.prepareViewControllerForAds(controller: controller)
                            
                            let deadline = DispatchTime.now() + DispatchTimeInterval.milliseconds(PlayersViewController.adsDelay)
                            DispatchQueue.main.asyncAfter(deadline: deadline){
                                self.initAds(url: adUrl, playerContext: playerId) { success, error in
                                    if success {
                                        os_log("Init Ads", log: DZDemoPlayer.demoplayer, type: .debug)
                                    }
                                    else {
                                        os_log("Init Ads failed", log: DZDemoPlayer.demoplayer, type: .debug)
                                    }
                                    
                                    controller.player?.play()
                                    
                                    let enableFlag = DemoAppData.shared.selectedPlayerId != nil && DemoAppData.shared.selectedPlayerId != ""
                                    self.enableControlls(flag: enableFlag)
                                }
                            }
                        }
                        else {
                            controller.player?.play()
                            
                            let enableFlag = DemoAppData.shared.selectedPlayerId != nil && DemoAppData.shared.selectedPlayerId != ""
                            self.enableControlls(flag: enableFlag)
                        }
                    }
                    else {
                        let errorMessage = error?.localizedDescription ?? "Error"
                        os_log("Init Player failed %s", log: DZDemoPlayer.demoplayer, type: .debug, errorMessage)
                    }
                })

#elseif AKAMAI
                DemoAppData.shared.framework?.initPlayer(player: player, playerView: playerContainerView, metadata: playerMetadata, { playerId, error in
                    if let playerId = playerId {
                        DemoAppData.shared.players[playerId] = player
                        DemoAppData.shared.controllers[playerId] = controller
                        
                        controller.player = player.player
                        
                        DispatchQueue.main.async {
                            self.addChildController(child: controller)
                            self.playersTableView.reloadData()
                        }
                        if let adUrl = self.selectedAdUrl {
                            let deadline = DispatchTime.now() + DispatchTimeInterval.milliseconds(PlayersViewController.adsDelay)
                            DispatchQueue.main.asyncAfter(deadline: deadline){
                                self.initAds(url: adUrl, playerContext: playerId) { success, error in
                                    if success {
                                        os_log("Init Ads", log: DZDemoPlayer.demoplayer, type: .debug)
                                    }
                                    else {
                                        os_log("Init Ads failed", log: DZDemoPlayer.demoplayer, type: .debug)
                                    }
                                    
                                    player.play(url: videoUrl)
                                    
                                    let enableFlag = DemoAppData.shared.selectedPlayerId != nil && DemoAppData.shared.selectedPlayerId != ""
                                    self.enableControlls(flag: enableFlag)
                                }
                            }
                        }
                        else {
                            player.play(url: videoUrl)
                            
                            let enableFlag = DemoAppData.shared.selectedPlayerId != nil && DemoAppData.shared.selectedPlayerId != ""
                            self.enableControlls(flag: enableFlag)
                        }
                    }
                })
                
#elseif BITMOVIN
                player.load(sourceConfig: sourceConfig)
                
                DemoAppData.shared.framework?.initPlayer(player: player, metadata: playerMetadata, { playerId, error in
                    if let playerId = playerId {
                        DemoAppData.shared.players[playerId] = player
                        DemoAppData.shared.controllers[playerId] = controller

                        let playerView = PlayerView(player: player, frame: .zero)
                        DemoAppData.shared.playerViews[playerId] = playerView
                        
                        DispatchQueue.main.async {
                            playerView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
                            playerView.frame = self.playerContainerView.bounds
                            
                            self.playerContainerView.addSubview(playerView)
                            self.playerContainerView.bringSubviewToFront(playerView)
                        
                            self.playersTableView.reloadData()
                        }
                        
                        player.play()
                        
                        let enableFlag = DemoAppData.shared.selectedPlayerId != nil && DemoAppData.shared.selectedPlayerId != ""
                        self.enableControlls(flag: enableFlag)
                    }
                })
#endif
            }

        }
        else {
            // change video in existing player
#if NATIVE
            if let selectedPlayerId = DemoAppData.shared.selectedPlayerId, let selectedPlayer = DemoAppData.shared.players[selectedPlayerId], let selectedController = DemoAppData.shared.controllers[selectedPlayerId] {
                let newVideoUrl = NSURL(string: videoUrl)! as URL
                let asset = AVAsset(url: newVideoUrl)
                let newPlayerItem = AVPlayerItem(asset: asset)
                
                selectedPlayer.replaceCurrentItem(with: newPlayerItem)
                selectedController.player?.play()
            }
#elseif AKAMAI
            if let selectedPlayerId = DemoAppData.shared.selectedPlayerId, let selectedPlayer = DemoAppData.shared.players[selectedPlayerId], let selectedController = DemoAppData.shared.controllers[selectedPlayerId] {
                selectedPlayer.play(url:videoUrl)
            }
#elseif BITMOVIN
//            if let selectedPlayerId = DemoAppData.shared.selectedPlayerId, let selectedPlayer = DemoAppData.shared.players[selectedPlayerId], let selectedController = DemoAppData.shared.controllers[selectedPlayerId] {
//                
//                
//        }
#endif
        }
    }

    @IBAction private func removePlayerButtonPressed(sender: UIButton) {
        if let selected = DemoAppData.shared.selectedPlayerId {
            DemoAppData.shared.framework?.deinitPlayer(playerContext: selected, { success, error in
                if let selectedController = DemoAppData.shared.controllers[selected] {
                    #if NATIVE || AKAMAI
                    selectedController.player?.pause()
                    #endif
                    selectedController.player = nil
                    
                    #if NATIVE || AKAMAI
                    self.removeChildController(child: selectedController)
                    
                    #elseif BITMOVIN
//                    self.bmPlayerView?.removeFromSuperview()
                    self.playerContainerView.subviews.forEach { subView in
                      subView.removeFromSuperview()
                    }
                    
                    DemoAppData.shared.playerViews[selected] = nil

                    #endif
                    DemoAppData.shared.players[selected] = nil
                    DemoAppData.shared.controllers[selected]  = nil
                    
                    DemoAppData.shared.selectedPlayerId = ""
                    self.playersTableView.reloadData()
                    
                    self.setNeedsFocusUpdate()
                    
                    let enableFlag = DemoAppData.shared.selectedPlayerId != nil && DemoAppData.shared.selectedPlayerId != ""
                    self.enableControlls(flag: enableFlag)
                }
            })
        }
    }
    
    @IBAction private func fullScreenButtonPressed(sender: UIButton) {
        if let selected = DemoAppData.shared.selectedPlayerId {
            #if NATIVE || AKAMAI
            if let selectedController = DemoAppData.shared.controllers[selected]{
                DispatchQueue.main.async() {
                    self.removeChildController(child: selectedController)
                    DemoAppData.shared.fullscreenFlag = true
                    selectedController.modalPresentationStyle = .fullScreen
                    
                    self.performSegue(withIdentifier: "PlayerFullScreenSegue", sender: nil)
                }
            }
            #elseif BITMOVIN
            
            #endif
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.destination is PlayerViewController {
            if let vc = segue.destination as? PlayerViewController,
               let selectedPlayerId = DemoAppData.shared.selectedPlayerId,
               let selectedPlayer = DemoAppData.shared.players[selectedPlayerId],
               let selectedController = DemoAppData.shared.controllers[selectedPlayerId] {
                vc.controller = selectedController
#if NATIVE
                vc.player = selectedPlayer
#elseif AKAMAI
                vc.player = selectedPlayer.player
#elseif BITMOVIN
                vc.player = selectedController.player
#endif
                vc.addChild(selectedController)
            }
        }
        else if segue.destination is VideoTypeViewController {
            if let vc = segue.destination as? VideoTypeViewController {
                vc.selectedVideoType = selectedVideoType
                vc.playersController = self
                DemoAppData.shared.displayModelsFlag = true
            }
        }
        else if segue.destination is AdTypeViewController {
            if let vc = segue.destination as? AdTypeViewController {
                vc.selectedAdType = selectedAdType
                vc.playersController = self
                DemoAppData.shared.displayModelsFlag = true
            }
        }
        else if segue.destination is CustomMetadataViewController {
            if let vc = segue.destination as? CustomMetadataViewController {
                if customMetadataSelector == 1 {
                    vc.mainTitle = "Player Metadata"
                    if let selected = DemoAppData.shared.selectedPlayerId {
                        vc.customData = DemoAppData.shared.framework?.getPlayerCustomMetadata(playerContext:selected) ?? [:]
                    }
                }
                else if customMetadataSelector == 2 {
                    vc.mainTitle = "Session Metadata"
                    vc.customData = DemoAppData.shared.framework?.getSessionCustomMetadata() ?? [:]
                }
                vc.playersController = self
                DemoAppData.shared.displayModelsFlag = true
            }
        }
        else if segue.destination is CustomEventViewController {
            if let vc = segue.destination as? CustomEventViewController {
                vc.customEventName = DemoAppData.shared.customEventName
                vc.customEventData = DemoAppData.shared.customEventData
                vc.playersController = self
                DemoAppData.shared.displayModelsFlag = true
            }
        }
    }
}
        
extension PlayersViewController {
    func initMetadata(){
//        DemoAppData.shared.framework.initCustomMetadata(playerMetadata, sessionMetadata)
    }
}


extension PlayersViewController:AVPlayerViewControllerDelegate {
    #if os(iOS)
    func playerViewController(_ playerViewController: AVPlayerViewController, willBeginFullScreenPresentationWithAnimationCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        print("enter")
        }
    #endif
}

extension PlayersViewController {
    func isSelectedAdLinear() -> Bool {
        switch (self.selectedAdType) {
        case DZAdType.singleInlineLinear, DZAdType.singleVPAID20Linear,
            DZAdType.singleSkippableInline, DZAdType.singleRedirectLinear:
            return true
        default:
            return false
        }
    }
    
    func prepareViewControllerForAds(controller: AVPlayerViewController) {
        if (isSelectedAdLinear()) {
            controller.requiresLinearPlayback = true
        }
    }
}

extension PlayersViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DemoAppData.shared.players.keys.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellRowIdentifier = "PlayersCellIdentifier"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellRowIdentifier)
        
        let keys: [String] = Array<String>(DemoAppData.shared.players.keys)
        let key: String = keys[indexPath.row]
        cell?.textLabel?.text = key
        
        if DemoAppData.shared.selectedPlayerId == key {
           cell?.accessoryType = .checkmark
        }
        else {
            cell?.accessoryType = .none
        }
       
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let keys: [String] = Array<String>(DemoAppData.shared.players.keys)
        let key: String = keys[indexPath.row]
        
        if key == DemoAppData.shared.selectedPlayerId! {
            DemoAppData.shared.selectedPlayerId = ""
            
            DispatchQueue.main.async() {
                let enableFlag = DemoAppData.shared.selectedPlayerId != nil && DemoAppData.shared.selectedPlayerId != ""
                self.enableControlls(flag: enableFlag)
                
                self.playersTableView.reloadData()
            }
        }
        else {
            if let selectController = DemoAppData.shared.controllers[key] {
                DispatchQueue.main.async() {
                    #if NATIVE || AKAMAI
                    if let selectedId = DemoAppData.shared.selectedPlayerId, let prevSelectController = DemoAppData.shared.controllers[selectedId] {
                        self.removeChildController(child: prevSelectController)
                    }
                    self.addChildController(child: selectController)
                    #elseif BITMOVIN
                    if let selectedId = DemoAppData.shared.selectedPlayerId, let prevSelectView = DemoAppData.shared.playerViews[selectedId] {
                        prevSelectView.removeFromSuperview()
                    }
                    
                    if let selectedView = DemoAppData.shared.playerViews[key] {
                        self.playerContainerView.addSubview(selectedView)
                        self.playerContainerView.bringSubviewToFront(selectedView)
                    }
                    #endif
                    DemoAppData.shared.selectedPlayerId = key

                    let enableFlag = DemoAppData.shared.selectedPlayerId != nil && DemoAppData.shared.selectedPlayerId != ""
                    self.enableControlls(flag: enableFlag)
                    
                    self.playersTableView.reloadData()
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if cell.responds(to: #selector(setter: UIView.preservesSuperviewLayoutMargins)) {
            cell.preservesSuperviewLayoutMargins = false
        }
        
        if cell.responds(to: #selector(setter: UIView.layoutMargins)) {
            cell.layoutMargins = UIEdgeInsets.zero
        }
    }
}
