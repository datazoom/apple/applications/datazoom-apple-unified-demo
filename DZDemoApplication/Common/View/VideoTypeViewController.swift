//
//  VideoTypeViewController.swift
//  DZNativeDemoApplication
//
//  Created by Vuk on 27.11.21..
//

import Foundation
import UIKit

import DZDemoApplicationCommon

class VideoTypeViewController: UIViewController {

    let videoTypes: [DZVideoType] = DZVideoType.allCases
    var playersController: PlayersViewController? = nil
    var selectedVideoType : DZVideoType = DZVideoType.defaultType
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        DemoAppData.shared.displayModelsFlag = false

        super.viewWillDisappear(animated)
    }
    
}

extension VideoTypeViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return videoTypes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellRowIdentifier = "VideoTypeCellIdentifier"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellRowIdentifier)
        #if os(iOS)
        cell?.textLabel?.font = UIFont.systemFont(ofSize: 18.0)
        #endif
        
        let videoType = videoTypes[indexPath.row]
        cell?.textLabel?.text = videoType.title

        
        if selectedVideoType == videoType {
           cell?.accessoryType = .checkmark
        }
        else {
            cell?.accessoryType = .none
        }
       
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedVideoType = videoTypes[indexPath.row]
        playersController?.onVideoTypeChanged(type: selectedVideoType)
        self.dismiss(animated: true, completion: nil)
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if cell.responds(to: #selector(setter: UIView.preservesSuperviewLayoutMargins)) {
            cell.preservesSuperviewLayoutMargins = false
        }
        
        if cell.responds(to: #selector(setter: UIView.layoutMargins)) {
            cell.layoutMargins = UIEdgeInsets.zero
        }
    }
}

