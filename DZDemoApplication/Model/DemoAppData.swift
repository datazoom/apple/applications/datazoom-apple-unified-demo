//
//  DemoAppData.swift
//  DZDemoApplication
//
//  Created by Ugljesha on 31.5.22..
//

import Foundation
import UIKit
import AVKit
import AVFoundation
import os

import DZDemoApplicationCommon
import DZDemoApplicationCommonIMA

#if AKAMAI
import AmpCore
#elseif BITMOVIN
import BitmovinPlayerCore
#endif

import DZ_Collector

#if canImport(GoogleInteractiveMediaAds)
import GoogleInteractiveMediaAds
#endif

public class DemoAppData: NSObject {
    static let shared = DemoAppData()

    var selectedPlayerId: String?
    
    var fullscreenFlag: Bool = false
    var displayModelsFlag: Bool = false

    var framework: DZCollector? = nil
    
#if NATIVE
    var players: [String: AVPlayer] = [:]
#elseif AKAMAI
    var players: [String: AmpPlayer] = [:]
#elseif BITMOVIN
    var players: [String: Player] = [:]
    var playerViews: [String: PlayerView] = [:]
#endif
    var controllers: [String: AVPlayerViewController] = [:]

    var playerConfiguration : DZPlayerConfiguration = DZPlayerConfiguration()
    
    var customEventName:String = ""
    var customEventData:[String: Any] = [:]
    var playerCutomMetadata:[String: Any]? = [:]
    
    var ads:[String: DZAdDataIMA]? = [:]
    
    public func initData() {
        self.players = [:]
        self.controllers = [:]
        self.selectedPlayerId = ""
        self.fullscreenFlag = false
        
        self.playerConfiguration = DZPlayerConfiguration()
        
        self.customEventName = ""
        self.customEventData = [:]
        self.playerCutomMetadata = [:]
        
        self.ads = [:]
    }
    
    public func clearData() {
        if players.count > 0 {
            let keys: [String] = Array<String>(players.keys)
            for key in keys {
                let player = players[key]
                //player?.pause()
                framework?.deinitPlayer(playerContext: key) {_,_ in
                }
            }
        }
        
        self.players = [:]
        self.controllers = [:]
        self.selectedPlayerId = ""
        self.fullscreenFlag = false
        
        self.playerConfiguration = DZPlayerConfiguration()
        
        self.customEventName = ""
        self.customEventData = [:]
        self.playerCutomMetadata = [:]
        
        self.ads = [:]
    }
}
